# -*- coding:utf-8 -*-

import os
import time
import numpy as np
import tensorflow as tf
import data_helpers
from gensim.models import Word2Vec

logger = data_helpers.logger_fn('tflog', 'test-{}.log'.format(time.asctime()))

MODEL_DIR = '/home/j/Documents/ops_review/django_kdd/filter/nameko/text/1522197229'

tf.flags.DEFINE_string("checkpoint_dir", MODEL_DIR, "Checkpoint directory from training run")

# Model Hyperparameters
tf.flags.DEFINE_integer("pad_seq_len", 1500, "Recommand padding Sequence length of data (depends on the data)")
tf.flags.DEFINE_integer("embedding_dim", 200, "Dimensionality of character embedding (default: 128)")
tf.flags.DEFINE_integer("embedding_type", 1, "The embedding type (default: 1)")
tf.flags.DEFINE_string("filter_sizes", "3,4,5", "Comma-separated filter sizes (default: '3,4,5')")
tf.flags.DEFINE_integer("num_filters", 128, "Number of filters per filter size (default: 128)")
tf.flags.DEFINE_float("dropout_keep_prob", 0.5, "Dropout keep probability (default: 0.5)")
tf.flags.DEFINE_float("l2_reg_lambda", 0.0, "L2 regularization lambda (default: 0.0)")

# Misc Parameters
tf.flags.DEFINE_boolean("allow_soft_placement", True, "Allow device soft device placement")
tf.flags.DEFINE_boolean("log_device_placement", False, "Log placement of ops on devices")
tf.flags.DEFINE_boolean("gpu_options_allow_growth", True, "Allow gpu options growth")

FLAGS = tf.flags.FLAGS
FLAGS._parse_flags()
dilim = '-' * 100
logger.info('\n'.join([dilim, *['{:>50}|{:<50}'.format(attr.upper(), value)
                                for attr, value in sorted(FLAGS.__flags.items())], dilim]))

word2vec_file = '/home/j/Documents/ops_review/django_kdd/wiki.zh.text.model'

# Load word2vec model file
if os.path.isfile(word2vec_file):
    model = Word2Vec.load(word2vec_file)

def text_classification_api(text_list):
    """Test CNN model."""


    test_data = data_helpers.load_data_without_label(model,text_list,FLAGS.pad_seq_len)

    # logger.info('✔︎ Test data padding...')

    x_test_front, y_test = data_helpers.pad_data(test_data, FLAGS.pad_seq_len)

    # Build vocabulary
    VOCAB_SIZE = data_helpers.load_vocab_size(FLAGS.embedding_dim)
    # pretrained_word2vec_matrix = data_helpers.load_word2vec_matrix(VOCAB_SIZE, FLAGS.embedding_dim)

    # Load cnn model
    logger.info("✔ Loading model...")
    checkpoint_file = tf.train.latest_checkpoint(FLAGS.checkpoint_dir)
    logger.info(checkpoint_file)

    graph = tf.Graph()
    with graph.as_default():
        session_conf = tf.ConfigProto(
            allow_soft_placement=FLAGS.allow_soft_placement,
            log_device_placement=FLAGS.log_device_placement)
        session_conf.gpu_options.allow_growth = FLAGS.gpu_options_allow_growth
        sess = tf.Session(config=session_conf)
        with sess.as_default():
            # Load the saved meta graph and restore variables
            saver = tf.train.import_meta_graph("{}.meta".format(checkpoint_file))
            saver.restore(sess, checkpoint_file)

            # Get the placeholders from the graph by name
            input_x_front = graph.get_operation_by_name("input_x_front").outputs[0]
            # input_x_behind = graph.get_operation_by_name("input_x_behind").outputs[0]

            # input_y = graph.get_operation_by_name("input_y").outputs[0]
            dropout_keep_prob = graph.get_operation_by_name("dropout_keep_prob").outputs[0]

            # pre-trained_word2vec
            pretrained_embedding = graph.get_operation_by_name("embedding/W").outputs[0]

            # Tensors we want to evaluate
            scores = graph.get_operation_by_name("output/scores").outputs
            predictions = graph.get_operation_by_name("output/predictions").outputs[0]
            softmaxScores = graph.get_operation_by_name("output/softmaxScores").outputs[0]
            topKPreds = graph.get_operation_by_name("output/topKPreds").outputs[0]

            # Generate batches for one epoch
            # batches = data_helpers.batch_iter(list(zip(x_test_front, x_test_behind)), FLAGS.batch_size, 1,
            #                                   shuffle=False)

            # Collect the predictions here
            all_scores = []
            all_softMaxScores = []
            all_predictions = []
            all_topKPreds = []

            # for x_test_batch in batches:
            if True:
                # x_batch_front, x_batch_behind = zip(*x_test_batch)
                # x_batch_front, x_batch_behind=(x_test_front, x_test_behind)
                feed_dict = {
                    input_x_front: x_test_front,
                    # input_x_behind: x_batch_behind,
                    dropout_keep_prob: 1.0
                }
                batch_scores = sess.run(scores, feed_dict)
                all_scores = np.append(all_scores, batch_scores)

                batch_softmax_scores = sess.run(softmaxScores, feed_dict)
                all_softMaxScores = np.append(all_softMaxScores, batch_softmax_scores)

                batch_predictions = sess.run(predictions, feed_dict)
                all_predictions = np.concatenate([all_predictions, batch_predictions])

                batch_topKPreds = sess.run(topKPreds, feed_dict)
                all_topKPreds = np.append(all_topKPreds, batch_topKPreds)

            # print(all_predictions, all_topKPreds, all_softMaxScores)

    return all_predictions,all_topKPreds
    logger.info("✔ Done.")


if __name__ == '__main__':
    for ind in range(1,12893,400):
        text_list=[]
        lable_dict={}
        lable_list=[]
        index_list=[]

        with open('text_label') as f:
            for l in f.readlines():
                k,v=l.replace('\n','').split(',')
                lable_dict[k]=v

        for i in range(ind,ind+400):
            try:
                with open('./text/'+str(i)+'.txt') as f:
                    if lable_dict[str(i)] not in  ['12','01','02']:
                        text_list.append(f.read())
                        lable_list.append(lable_dict[str(i)])
                        index_list.append(i)
            except FileNotFoundError:
                pass
        predict_list,topK_list=text_classification_api(text_list)
        for i in range(0,len(text_list)):
            if int(lable_list[i])!=int(predict_list[i]) and int(lable_list[i])==0:
                print(index_list[i],lable_list[i],predict_list[i],topK_list[i])
