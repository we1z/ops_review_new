# -*- coding:utf-8 -*-

import os
import multiprocessing
import gensim
import logging
import json
import numpy as np
import matplotlib.pyplot as plt
import jieba

from pylab import *
from gensim.models import Word2Vec
from tflearn.data_utils import to_categorical, pad_sequences
import random
import re

TEXT_DIR = 'content.txt'


def logger_fn(name, file, level=logging.INFO):
    tf_logger = logging.getLogger(name)
    tf_logger.setLevel(level)
    fh = logging.FileHandler(file, mode='w')
    formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s')
    fh.setFormatter(formatter)
    tf_logger.addHandler(fh)
    return tf_logger


def create_word2vec_model(embedding_size, input_file=TEXT_DIR):
    """
    Create the word2vec model based on the given embedding size and the corpus file.
    :param embedding_size: The embedding size
    :param input_file: The corpus file
    """
    word2vec_file = 'word2vec_' + str(embedding_size) + '.model'

    if os.path.isfile(word2vec_file):
        logging.info('☛ The word2vec model you want create already exists!')
    else:
        sentences = word2vec.LineSentence(input_file)
        # sg=0 means use CBOW model(default); sg=1 means use skip-gram model.
        model = gensim.models.Word2Vec(sentences, size=embedding_size, min_count=0,
                                       sg=0, workers=multiprocessing.cpu_count())
        model.save(word2vec_file)


def load_vocab_size(embedding_size):
    """
    Return the vocab size of the word2vec file.
    :param embedding_size: The embedding size
    :return: The vocab size of the word2vec file
    """
    word2vec_file = '/home/hins/Documents/zhwiki-latest-pages-articles/wiki.zh.text.model'

    if os.path.isfile(word2vec_file):
        model = Word2Vec.load(word2vec_file)
        return len(model.wv.vocab.items())
    else:
        logging.info("✘ The word2vec file doesn't exist. "
                     "Please use function <create_vocab_size(embedding_size)> to create it!")


def data_word2vec(word2vec_model,max_seq_len):
    """
    Create the research data tokenindex based on the word2vec model file.
    Returns the class Data(includes the data tokenindex and data labels).
    :param word2vec_model: The word2vec model file
    :return: The class Data(includes the data tokenindex and data labels)
    """
    vocab = dict([(k, v.index) for (k, v) in word2vec_model.wv.vocab.items()])

    def token_to_index(content):
        result = []
        for item in content.split(" "):
            id = vocab.get(item)
            if id is None:
                id=0

                if not item:
                    id=3492

                # 对感兴趣的数字进行特殊处理
                elif re.match('^\d+(年|月|日)$', item):
                    id = 9143
                elif re.match('^\d+\.\d+$', item):
                    id = 39938
                elif re.match('^\d{1}$', item):
                    id = 888
                elif re.match('^\d{2}$', item):
                    id = 11376
                elif re.match('^\d{3}$', item):
                    id = 14637
                elif re.match('^\d{4}$', item):
                    id = 10537
                elif re.match('^\d+$', item):
                    id = 69384
                elif item == '%':
                    id = 28317
            result.append(id)
        # while len(result)<max_seq_len:
        #     result=result+result
        return result

    labels = []
    front_content_indexlist = []
    behind_content_indexlist = []

    flag_list = []

    with open('./text_label') as f:
        for l in f.readlines():
            flag_list.append(l.replace('\n', ''))

    # flag_list=flag_list[:4100]
    random.shuffle(flag_list)

    for l in flag_list:
        l_split = l.strip().split(',')
        if l_split[1] == '0':
            labels.append(array([1., 0., 0.]))
        elif l_split[1] == '1':
            labels.append(array([0., 1., 0.]))
        elif l_split[1] == '2':
            labels.append(array([0., 0., 1.]))
        elif l_split[1] in ['12','01','02']:
            continue
        else:
            continue
        with open('./text/' + l_split[0] + '.txt') as fin:
            content = ''.join(fin.readlines())
            content = ' ' + ' '.join(jieba.cut(content)) + ' '
            filtrate = re.compile(u'\s[^\u4E00-\u9FA50-9%.A-Za-z]+\s')

            content = filtrate.sub('  ', content)
            filtrate3 = re.compile(u'\s[^\u4E00-\u9FA50-9%.]+\s')
            content = filtrate3.sub('  ', content)
            content = content[1:-1]

        front_content_indexlist.append(token_to_index(content))

    class Data:
        def __init__(self):
            pass

        @property
        def number(self):
            return len(front_content_indexlist)

        @property
        def labels(self):
            return labels

        @property
        def front_tokenindex(self):
            return front_content_indexlist

    return Data()


def load_word2vec_matrix(vocab_size, embedding_size):
    """
    Return the word2vec model matrix.
    :param vocab_size: The vocab size of the word2vec model file
    :param embedding_size: The embedding size
    :return: The word2vec model matrix
    """
    word2vec_file = '/home/hins/Documents/zhwiki-latest-pages-articles/wiki.zh.text.model'

    if os.path.isfile(word2vec_file):
        model = Word2Vec.load(word2vec_file)
        vocab = dict([(k, v.index) for k, v in model.wv.vocab.items()])
        vector = np.zeros([vocab_size, embedding_size])
        for key, value in vocab.items():
            if len(key) > 0:
                vector[value] = model[key]
        return vector
    else:
        logging.info("✘ The word2vec file doesn't exist. "
                     "Please use function <create_vocab_size(embedding_size)> to create it!")


def load_data_and_labels(model,max_seq_len):

    data = data_word2vec(model,max_seq_len)
    logging.info('Found {} texts.'.format(data.number))

    return data


def load_data_without_label(word2vec_model,text_list,max_seq_len):
    """
    Create the research data tokenindex based on the word2vec model file.
    Returns the class Data(includes the data tokenindex and data labels).
    :param word2vec_model: The word2vec model file
    :param text_list:list of text to be test
    :return: The class Data(includes the data tokenindex)
    """
    vocab = dict([(k, v.index) for (k, v) in word2vec_model.wv.vocab.items()])

    def token_to_index(content):
        result = []
        for item in content.split(" "):
            id = vocab.get(item)
            if id is None:
                id=0

                if not item:
                    id=3492

                # 对感兴趣的数字进行特殊处理
                elif re.match('^\d+(年|月|日)$', item):
                    id = 9143
                elif re.match('^\d+\.\d+$', item):
                    id = 39938
                elif re.match('^\d{1}$', item):
                    id = 888
                elif re.match('^\d{2}$', item):
                    id = 11376
                elif re.match('^\d{3}$', item):
                    id = 14637
                elif re.match('^\d{4}$', item):
                    id = 10537
                elif re.match('^\d+$', item):
                    id = 69384
                elif item == '%':
                    id = 28317
            result.append(id)

        # while len(result)<max_seq_len:
        #     result=result+result
        return result

    labels = []
    front_content_indexlist = []
    behind_content_indexlist = []

    flag_list = []

    for content in text_list:

        content = ' '+ ' '.join(jieba.cut(content))+' '
        filtrate = re.compile(u'\s[^\u4E00-\u9FA50-9%.A-Za-z]+\s')

        content = filtrate.sub('  ', content)
        filtrate3 = re.compile(u'\s[^\u4E00-\u9FA50-9%.]+\s')
        content = filtrate3.sub('  ', content)
        content=content[1:-1]
        front_content_indexlist.append(token_to_index(content))

    class Data:
        def __init__(self):
            pass

        @property
        def number(self):
            return len(front_content_indexlist)

        @property
        def labels(self):
            return [0]* len(front_content_indexlist)

        @property
        def front_tokenindex(self):
            return front_content_indexlist

    return Data()


def pad_data(data, max_seq_len):
    """
    Padding each sentence of research data according to the max sentence length.
    Returns the padded data and data labels.
    :param data: The research data
    :param max_seq_len: The max sentence length of research data
    :returns: The padded data and data labels
    """
    data_front = pad_sequences(data.front_tokenindex, maxlen=max_seq_len, value=0.)
    # data_behind = pad_sequences(data.behind_tokenindex, maxlen=max_seq_len, value=0.)
    labels = data.labels
    return data_front, labels


def plot_seq_len(data, percentage=0.98):
    """
    Visualizing the sentence length of each data sentence.
    :param data_file: The data_file
    :param data: The class Data(includes the data tokenindex and data labels)
    :param percentage: The percentage of the total data you want to show
    """

    result = dict()
    for x in (data.front_tokenindex + data.behind_tokenindex):
        if len(x) not in result.keys():
            result[len(x)] = 1
        else:
            result[len(x)] += 1
    freq_seq = [(key, result[key]) for key in sorted(result.keys())]
    x = []
    y = []
    avg = 0
    count = 0
    border_index = []
    for item in freq_seq:
        x.append(item[0])
        y.append(item[1])
        avg += item[0] * item[1]
        count += item[1]
        if (count / 2) > data.number * percentage:
            border_index.append(item[0])
    avg = avg / (2 * data.number)
    logging.info('The average of the data sequence length is {}'.format(avg))
    logging.info('The recommend of padding sequence length should more than {}'.format(border_index[0]))
    xlim(0, 200)
    plt.bar(x, y)
    plt.savefig('histogram.png')
    plt.close()


def batch_iter(data, batch_size, num_epochs, shuffle=True):
    """
    含有 yield 说明不是一个普通函数，是一个 Generator.
    函数效果：对 data，一共分成 num_epochs 个阶段（epoch），在每个 epoch 内，如果 shuffle=True，就将 data 重新洗牌，
    批量生成 (yield) 一批一批的重洗过的data，每批大小是 batch_size，一共生成 int(len(data)/batch_size)+1 批。
    Generate a  batch iterator for a dataset.
    :param data: The data
    :param batch_size: The size of the data batch
    :param num_epochs: The number of epoches
    :param shuffle: Shuffle or not
    """
    data = np.array(data)
    data_size = len(data)
    num_batches_per_epoch = int((len(data) - 1) / batch_size) + 1
    for epoch in range(num_epochs):
        # Shuffle the data at each epoch
        if shuffle:
            shuffle_indices = np.random.permutation(np.arange(data_size))
            shuffled_data = data[shuffle_indices]
        else:
            shuffled_data = data
        for batch_num in range(num_batches_per_epoch):
            start_index = batch_num * batch_size
            end_index = min((batch_num + 1) * batch_size, data_size)
            yield shuffled_data[start_index:end_index]
