
import Levenshtein
import os


def is_similar_text(text_list, text):
    '''
    判断类型的新闻是否已经存在
    :param text_list:已经判断好的text列表
    :param text:需要判断的text
    :return:True or False
    '''
    if text in text_list:
        return True
    # if not text_list:
    #     return False
    # for i in text_list:
    #     distance = Levenshtein.distance(i, text)
    #     if distance < max(len(text), len(i)) - min(len(text), len(i)) * 1.2 or distance < 0.4 * max(
    #             len(text), len(i)):
    #         # print(text, i)
    #         return True
    # return False


file_name_list = os.listdir('./text')
file_text_list = []

# file_name_list.reverse()

with open('text_label') as f:
    for l in f.readlines():
        if l.split(',')[0]+'.txt' in file_name_list:
            print(l.replace('\n',''))

# for i in file_name_list:
#     with open('./text/' + i) as f:
#         text = f.read()
#         if is_similar_text(file_text_list, text):
#             print(i)
#             os.remove('./text/'+i)
#             pass
#         file_text_list.append(text)
        # if len(file_text_list)>50:
        #     del file_text_list[0]
