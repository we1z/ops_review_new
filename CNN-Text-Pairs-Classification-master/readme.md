基于cnn和word2vec的文本分类模型

源代码：https://github.com/RandolphVI/Text-Pairs-Relation-Classification

用于评审辅助系统，将媒体资讯分成三类：无用类，行业市场类，竞品分析类
calculate.py 可作为调用模型的接口
data_helpers.py 数据预处理相关函数
delete_similar_text.py 输出过于相近的训练语料，然后可手工删除
test_cnn.py 用于测试模型
train_cnn.py 用于训练模型

模型改进地方：
用中文维基百科和财经书籍数据训练word2vec模型
将源模型Pairs输入改成单篇文章输入
将%和数字作为特殊字符，赋以指定的word2vec向量
少量参数调整

备注：
百度网盘中有训练好的文本分类模型和word2vec模型
