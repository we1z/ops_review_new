文本摘要模型

通过seq2seq模型生成摘要

源代码
https://github.com/thunlp/TensorFlow-Summarization

改进地方：
1.修改参数
2.增加unk的类型
3.改成中文可用
4.将输入改成读取pkl文件,当目录存在docid.pkl和sumid.pkl文件时，则会直接读取文件，不需要读取数据库，提升速度
5.获取大量新闻摘要训练语料

目录说明：
/data/doc_dict.txt sum_dict.txt
原文和摘要的词袋模型，没有的时候会自动生成

/data/*
测试和验证的语料

/model/*
模型

test.py
测试模型用脚本

train.py
训练用脚本

配置要求：
普通16g机器，可以将batch_size设置为较小的数训练，具体需要测试，大概为4
百度云中模型是用阿里云的gpu服务器训练的
更少内存的机器很难训练

备注：
百度网盘中有已经训练好的模型和pkl文件
模型里面，test的时候beam_size为batch_size，应该是源代码的bug