ps基于微服务和深度学习的评审辅助系统

简述：该系统基于django，部署在服务器上，针对用户提交的关键词，用爬虫技术在网上搜索关键字的相关的金融数据，再通过深度学习方法将其分类，呈现给用户
程序入口为/filter/home

系统配置：
wotoudjango项目所用到的mongodb
redis(含redis-server)
shadowsocks
chromedriver
rabbitmq-server

pip3配置：
redis(redis的python封装接口)
django
newspapers-3k
nameko
selenium
pymongo
gensim
tensorflow==1.1
numpy
gunicorn
gevent
tflearn

启动：
1.确定数据库已经启动，否则
sudo mongod --fork --logpath /var/log/mongo.log --auth --wiredTigerCacheSizeGB 1

2.确定shadowsocks已经启动并能成功访问google，否则
nohup sslocal -c ~/ss1.config &
nohup sslocal -c ~/ss2.config &
nohup sslocal -c ~/ss3.config &
nohup sslocal -c ~/ss4.config &
nohup sslocal -c ~/ss5.config &
nohup sslocal -c ~/ss6.config &
nohup sslocal -c ~/ss7.config &

目前ss1.config的内容为
{"method":"aes-256-cfb","password":"sfzsmd1002","server":"202.144.195.238","server_port":"10576","local_port":1080}
ss2.config的内容为
{"method":"aes-256-cfb","password":"sfzsmd1002","server":"155.94.206.5","server_port":"10576","local_port":1081}
ss3.config的内容为
{"method":"aes-256-cfb","password":"sfzsmd1002","server":"172.96.200.39","server_port":"10576","local_port":1082}
ss4.config的内容为
{"method":"aes-256-cfb","password":"sfzsmd1002","server":"104.223.3.104","server_port":"10576","local_port":1083}
ss5.config的内容为
{"method":"aes-256-cfb","password":"sfzsmd1002","server":"116.251.223.192","server_port":"10576","local_port":1084}
ss6.config的内容为
{"method":"aes-256-cfb","password":"sfzsmd1002","server":"116.251.208.68","server_port":"10576","local_port":1085}
ss7.config的内容为
{"method":"aes-256-cfb","password":"sfzsmd1002","server":"139.59.186.54","server_port":"10576","local_port":1086}
为防ss失效，所以启动两个进程用多个端口

这些ss端口根据shadowsock而定，需要定期更新

3.确定redis-server，rabbitmq-server，nginx都已经启动
nginx配置文件中增加如下配置

http {
	server {
		listen 8080;
		server_name 106.75.65.56;
		charset UTF-8;
		root /home/yuanyuan/django_kdd;
		location / {
			proxy_pass http://127.0.0.1:8000;
            proxy_set_header Host $host;
            proxy_set_header X-Real-IP $remote_addr;
            proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
		}
	}
	
	server {
	listen 80 default_server;
	listen [::]:80 default_server;


	root /home/yuanyuan/django_kdd/result;

	index index.html index.htm index.nginx-debian.html;

	server_name _;

	location / {

		try_files $uri $uri/ =404;
	}

}
4.确认nameko/config.py配置是否正确，确认nameko/db_connection.py配置是否正确，是否需要增加数据库密码验证代码

5.cd到nameko目录
nohup nameko run --config ./config.yaml service &

6.cd到顶级的django_kdd目录
nohup gunicorn --worker-class=gevent django_kdd.wsgi:application &

备注：
1.django仅作为web入口，并没有用到其中mvc架构
2.主要逻辑在微服务框架nameko中实现
