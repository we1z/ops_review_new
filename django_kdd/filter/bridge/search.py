# coding=utf-8
"""
链接django和nameko的代码
"""

import os
import requests
import json
from django_kdd.settings import BASE_DIR


def search_api(keyword, company):
    '''
    django通过http链接发送消息给nameko，nameko返回搜索进度
    :param company: 公司名称
    :param keyword: 关键词
    :return: 无
    '''

    data = {'company': company, 'keyword': keyword}
    if os.path.exists('data/result/competitor-' + keyword + '.html'):
        requests.post('http://localhost:8006/nameko_percent', json=data)

    else:
        requests.post('http://localhost:8006/nameko_dispatch_keyword', json=data)
        requests.post('http://localhost:8006/nameko_percent', json=data)

