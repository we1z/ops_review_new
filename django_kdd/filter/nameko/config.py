'''
配置文件
'''
import redis
import logging

# 文本分类模型所在
MODEL_DIR = '/home/yuanyuan/django_kdd/filter/nameko/text/runs/1526869804/checkpoints/'
# 文摘模型所在路径
# checkpoint="/home/yuanyuan/django_kdd/filter/nameko/model/"
# word2vec所在
word2vec_file = '/data/word2vec_data_small/wiki.zh.text.model'
#lda模型所在路径
# lda_model_path='/home/yuanyuan/django_kdd/filter/nameko/lda_model/lda_model'
# 写docx文件路径
# docx_path='/data/summarization/'
# 写html文件路径
write_path = '/data/result/'
# 已经可以翻墙的端口
socks5_port=['1080','1081','1082','1083','1084','1085','1086','1087','1088','1089','1090']
current_socks5_port_index=0
# redis链接
pool = redis.ConnectionPool(host='localhost', port=6379, db=0)
redis_conn = redis.StrictRedis(connection_pool=pool)
# logging文件配置
logging.basicConfig(format='%(asctime)s %(message)s')
logger=logging.getLogger('nameko')


# learning_rate=1.0
# size=1024
# embsize=200
# num_layers=1
# data_dir="/home/yuanyuan/django_kdd/filter/nameko/data"

# # 用于存放训练好的模型的位置
# tfboard="tfboard"
# decode=True
# geneos=True
# max_gradient=1.0
# batch_size=1
# doc_vocab_size=32500
# sum_vocab_size=30000
# max_train=0
# max_iter=1000000
# steps_per_validation=1000
# steps_per_checkpoint= 100
#
# # We use a number of buckets for sampling
# MARK_PAD = "<PAD>"
# MARK_UNK1 = "<UNK1>"
# MARK_UNK2 = "<UNK2>"
# MARK_UNK3 = "<UNK3>"
# MARK_UNK4 = "<UNK4>"
# MARK_UNK5 = "<UNK5>"
# MARK_UNK6 = "<UNK6>"
# MARK_EOS = "<EOS>"
# MARK_GO = "<GO>"
# MARKS = [MARK_PAD, MARK_UNK1, MARK_UNK2, MARK_UNK3, MARK_UNK4, MARK_UNK5, MARK_UNK6, MARK_EOS, MARK_GO]
# ID_PAD = 0
# ID_UNK1 = 1
# ID_UNK2 = 2
# ID_UNK3 = 3
# ID_UNK4 = 4
# ID_UNK5 = 5
# ID_UNK6 = 6
# ID_EOS = 7
# ID_GO = 8
