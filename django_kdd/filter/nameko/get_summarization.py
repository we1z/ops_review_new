# -*- coding: utf-8 -*

from summarization import decode_1
from docx import Document
from docx.shared import Inches
import difflib
from datetime import datetime
import copy
import jieba
import re
from db_connection import mongodb_connection
from k_mean_classification import get_classified_id
import requests
import html
import os
import shutil
from config import *
import sys
from multiprocessing import Pool
from urllib.parse import unquote


def download_pic(para):
    try:
        with open(para[0], 'wb') as f:
            f.write(requests.get(url=para[1], timeout=10).content)
        print('保存图片：{},图片下载地址：{}'.format(para[0], para[1]))
    except:
        print("图片下载失败。")
        pass



def get_txt_from_html(search_keyword,type):
    """
    从html文件中生成txt文件，并且收集其中的图片
    :param filepath:html文件的路径
    :return:生成txt文件和图片集
    """

    raw_content=''.join([i['article_html'] for i in mongodb_connection('corpus')['classification'].find({'keyword':search_keyword,'type':type})])
    content=html.unescape(raw_content)
    pattern=re.compile('(iframe{.*?})',re.S)
    content=re.sub(pattern,'',content)
    pattern=re.compile('(img {.*?})',re.S)
    content=re.sub(pattern,'',content)
    pattern=re.compile('>(.*?)<|src="(.*?)"', re.S)
    p=[max(i).strip() for i in re.findall(pattern, content) if max(i).strip() != '']

    os.makedirs(os.getcwd()+'/pic/{}'.format(search_keyword+'-'+str(type)))
    para=[]
    for i in p:
        if i.startswith('//'):
            url = 'http:'+i
            pic_num=p.index(i)
            # content=requests.get(url=url,timeout=10).content
            filepath=os.getcwd()+'/pic/{}/{}.png'.format(search_keyword+'-'+str(type),pic_num)
            para.append([filepath,url])
            # with open(filepath,'wb') as f:
            #     f.write(content)
            # print('保存图片：{},图片下载地址：{}'.format(filepath, url))
        elif i.startswith('http'):
            pic_num=p.index(i)
            # content=requests.get(url=i,timeout=10).content
            filepath=os.getcwd()+'/pic/{}/{}.png'.format(search_keyword+'-'+str(type),pic_num)
            para.append([filepath,i])
            # with open(filepath,'wb') as f:
            #     f.write(content)
            # print('保存图片：{},图片下载地址：{}'.format(filepath, url))
    pool=Pool(processes=16)
    pool.map(download_pic,para)

    final = []

    for i in p:
        if re.match('[0-9a-zA-Z\_\./\-:]{8,80}', i):
            location = "&&&"+str(p.index(i))+"&&&"
            final.append('。'+location+'。')
        else:
            final.append(i)
    total_texts = ''.join(final)
    # 保存原始信息
    mongodb_connection('corpus')['summarization'].insert({'keyword':search_keyword,'type':type,'html_test':total_texts})
    # 过滤原始信息中过短的句子
    total_text = total_texts.split('。')
    final_washed = []
    for i in total_text:
        if len(i) > 50 or re.match('&&&[\d]+&&&', i):
            final_washed.append(i)
    return final_washed



def get_corpus_from_html(search_keyword,type):
    """
    从txt文件中提取文档，将原始文档存在origin_path中的txt文档中，处理后的文档存在test_path中的txt文件中
    :param original_path: 存放原始文档的文件路径
    :param test_path: 处理后的文档存放文件，用于输入到文摘模型中
    :param skip_num: 跳过的文档数量
    :param num: 测试文档数量
    :return:
    """
    html_content = get_txt_from_html(search_keyword,type)
    i = '。'.join(html_content)

    # 带有数字的处理文本，实现下一步的数字恢复
    original_txt = ''
    # 将数字处理为#后的文本，输入seq2seq模型中
    # processed_txt = open(test_path, 'w', encoding='utf-8')
    processed_txt=''
    # 数据库读出的原始文本

    content_text = i.replace('\u200b', '')
    content_text = re.sub('^\s*', '', content_text)
    content_text = re.sub('\s*$', '', content_text)
    content_text = re.sub('^\n*', '', content_text)
    content_text = re.sub('\n*$', '', content_text)
    content_text = re.sub('\s+', ' ', content_text)
    content_text = re.sub('&amp;', '&', content_text)
    content_text = re.sub('&lt;', '<', content_text)
    content_text = re.sub('&gt;', '>', content_text)
    content_text = content_text.replace('１', '1').replace('２', '2').replace('３', '3').replace('４', '4')\
        .replace('５', '5').replace('６', '6').replace('７', '7').replace('８', '8').replace('９', '9')\
        .replace('０', '0').replace('％', '%')
    mongodb_connection('corpus')['summarization'].update({'keyword': search_keyword, 'type': type},{'$set':{'database_txt':content_text}})
    content_text = re.sub('&&&[\d]+&&&。', '', content_text)
    tmp_content_text = content_text.split('。')
    k = len(tmp_content_text)//20
    class_num = min(10, max(k, 3))
    classifed_id = get_classified_id(content_text, class_num, 2)


    for item in classifed_id:
        c = ''
        for ii in classifed_id[item]:
            c = c + tmp_content_text[ii]+'。'
        c = " ".join(jieba.cut(c))

        c = c.replace('    ', ' ').replace('   ', ' ').replace('  ', ' ').replace('  ', ' ')
        c = c.replace('\n', '<NL>')
        while "<NL> <NL>" in c:
            c = c.replace("<NL> <NL>", "<NL>")
        c = c.strip()

        while c.startswith("<NL> "):
            c = c[5:]
        original_txt+=c+'\n'
        c = re.sub('\d', '#', c)
        processed_txt+=c+'\n'
    mongodb_connection('corpus')['summarization'].update({'keyword': search_keyword, 'type': type},{'$set':{'original_txt':original_txt}})
    mongodb_connection('corpus')['summarization'].update({'keyword': search_keyword, 'type': type},{'$set':{'processed_txt':processed_txt}})



def get_range(word_location, word_locations, half_range_of_window, word_num_of_abstract):
    """
    生成模式串
    :param word_location:当前字符在摘要中的位置
    :param word_locations：所有待替换字符的位置
    :param half_range_of_window：设置模式串大小
    :param word_num_of_abstract:摘要词数
    :rtype: list
    """
    range_of_list = []
    range_of_list.append(word_location)
    tmp = 0
    for i in range(half_range_of_window):
        former = word_location-i-1-tmp
        former_less = 0
        flag = True
        while flag:
            if former >= 0 and former not in word_locations:
                range_of_list.append(former)
                flag = False
            elif former < 0:
                former_less = half_range_of_window-i
                break
            else:
                range_of_list.append(former)
                former -= 1
                tmp += 1
        if former < 0:
            break
    tmp = 0
    for i in range(half_range_of_window + former_less):

        latter = word_location + i + 1 + tmp
        latter_less = 0
        flag = True

        while flag:
            if latter < word_num_of_abstract and latter not in word_locations:
                range_of_list.append(latter)
                flag = False
            elif latter >= word_num_of_abstract:
                latter_less = half_range_of_window - i
                break
            else:
                range_of_list.append(latter)
                latter += 1
                tmp += 1
        if latter > word_num_of_abstract:
            break
    tmp = 0
    for i in range(half_range_of_window, half_range_of_window + latter_less):
        former = word_location-i-1-tmp
        while flag:
            if former >= 0 and former not in word_locations:
                range_of_list.append(former)
                flag = False
            elif former < 0:
                former_less = half_range_of_window-i
                break
            else:
                range_of_list.append(former)
                former -= 1
                tmp += 1
        if former < 0:
            break
    range_of_list.sort()
    return range_of_list


def get_similarity(target, compared_list):
    """
    calculate the similarity between the target sentence and compared sentence
    :param target:包含未知字符的模式串
    :param compared_list:目标串
    :rtype :float: 两个字符的相似度
    """
    same_num = 0
    for i in range(len(target)):
        if target[i] == compared_list[i]:
            same_num += 1
    return (same_num+1)/len(target)


def get_recover_result(search_keyword,type):
    """
    恢复生成文摘中被替换的unk符号和被替换的数字
    :param original_path: 存放包含数字处理文本的地址
    :param output_path: seq2seq模型输出 结果的存放地址
    :param abstract_complete_path: 恢复完成后的文本存放地址
    :return: none
    """
    original = ''.join([i['original_txt'] for i in mongodb_connection('corpus')['summarization'].find({'keyword': search_keyword, 'type': type})]).split('\n')
    abstract = ''.join([i['output'] for i in mongodb_connection('corpus')['summarization'].find({'keyword': search_keyword, 'type': type})]).split('\n')

    half_windows_range = 9

    file_complete=''
    for i in range(len(abstract)):
        original_list = original[i].split(' ')
        abstract_list = abstract[i].split(' ')
        replace = []
        for index, item in enumerate(abstract_list):
            if '#' in item:
                replace.append((item, index))
            if 'UNK' in item:
                replace.append((item, index))
        replace.sort(key=lambda x: x[1])
        word_locations = list(map(lambda x: x[1], replace))
        real_word = []
        for j in range(len(replace)):
            list_num = get_range(replace[j][1], word_locations, half_windows_range, len(abstract_list))

            target = [abstract_list[item] for item in list_num]
            length = len(target)
            length_total = len(original_list)
            result = []
            for k in range(length_total-length):
                compared_list = original_list[k:k+length]
                similarity = get_similarity(compared_list, target)
                result.append((similarity, compared_list[list_num.index(replace[j][1])]))
            result.sort(key=lambda x: x[0], reverse=True)
            real_word.append(result[0][1])
        complete_abstract = copy.copy(abstract_list)
        for k in range(len(real_word)):
            complete_abstract[replace[k][1]] = real_word[k]
        a = ' '.join(complete_abstract)
        file_complete=file_complete+a

    mongodb_connection('corpus')['summarization'].update({'keyword': search_keyword, 'type': type},{'$set':{'abstract_complete':file_complete}})



def get_regular_content(search_keyword,type):
    """
    extract important content from the original text according designed regular
    :return: regular summarization
    """
    result = []
    pic_num = 0
    content = [i['database_txt'] for i in mongodb_connection('corpus')['summarization'].find({'keyword': search_keyword, 'type': type})][0]
    re.sub(r'\n', '', content)
    contents = content.split('。')
    img_regex = re.compile(r'&&&[\d]+&&&')
    except_regex = re.compile(r'“|”|可持续发展|历史')
    chn_regex = re.compile(r'因此|但是|[一二三四五六七八九][是|、]'
                           r'|第[一二三四五六七八九]|\d+、|侧面|上市|兼并|大型|巨头|市场报告|营业额|共\d+家企业|领军'
                           r'|同比[增长|减少]|寡头|垄断|竞争|政策|需求|供给')
    punctuation_regex = re.compile(r'、')
    dig_regex = re.compile(r'\d+\.\d+|\d+')
    date_regex = re.compile(r'\d+年|\d+月|\d+日|\d+-\d+-\d+|\d+:\d+:\d|\d+:\d+')

    for sentence in contents:
        chn_res = chn_regex.findall(sentence)

        dig_res = dig_regex.findall(sentence)
        date_res = date_regex.findall(sentence)


        if except_regex.match(sentence):
            continue

        if img_regex.match(sentence):
            result.append(sentence)
            pic_num += 1
            continue
        if len(chn_res) >= 1:
            result.append(sentence)
            continue

        num_of_data = 0
        for date in date_res:
            num_of_data += len(dig_regex.findall(date))
        if len(dig_res)-num_of_data >= 2:
            result.append(sentence)
            continue

        for item in sentence.split(','):
            punctuation_res = punctuation_regex.findall(item)
            if 4 > len(punctuation_res) > 2:
                result.append(sentence)
                break

    return result, pic_num

def generate_doc(docs,search_keyword,type):
    """
    生成文档
    :param doc:str 文档
    :param path:存放文件名称
    :return:
    """
    print("开始写docx。")
    model_summarization = [i['abstract_complete'] for i in mongodb_connection('corpus')['summarization'].find({'keyword': search_keyword, 'type': type})][0]

    model_summarization.replace('\n', '')
    model_summarization_split = model_summarization.split('。')

    doc = docs.split('。')
    index = 0
    document = Document()
    doc_name = search_keyword+'-'+str(type)
    document.add_heading('Summarization-'+doc_name, 0)
    for item in doc:
        if re.match(r'&&&[\d]+&&&', item):
            target_name = item.replace('&','')
            files = os.listdir(os.getcwd()+r'/pic/{}'.format(search_keyword+'-'+str(type)))
            for file in files:
                file_name = file.split('.')[0]
                if file_name == target_name:
                    pic_path = os.getcwd()+'/pic/{}/'.format(search_keyword+'-'+str(type))+file
                    try:
                        document.add_picture(pic_path, width=Inches(5))
                    except:
                        pass

        else:
            index += 1
            max_similarity = 0
            for sen in model_summarization_split:
                ratio = difflib.SequenceMatcher(None, sen, item).ratio()
                max_similarity = max(max_similarity, ratio)
            if max_similarity > 0.7:
                p = document.add_paragraph()
                p.add_run(item).bold = True
            else:
                document.add_paragraph(item)

    save_file = docx_path+doc_name+'.docx'
    document.save(save_file)



def summarization_recover_api(search_keyword,type):
    """
    恢复摘要并打印
    :return:
    :return:
    """

    print("获得语料，进行预处理，将结果保存到指定文件中。")
    get_corpus_from_html(search_keyword,type)
    print("将处理后的内容输入到seq2seq模型中，获取模型摘要。")
    decode_1(search_keyword,type)

    print("恢复摘要中被替换掉的数字，将恢复结果保存到指定位置中去。")
    get_recover_result(search_keyword,type)



def generate_total_summarization(search_keyword,type):
    """
    将模型生成的文摘和规则提取的文摘合并，去除同类项
    :param skip_num: 跳过的文章数，如果skip_num不是int，表示从html中获取文本
    :param reload_model: 是否更换测试数据
    :return: 整理后的文摘，此时文摘无序
    """
    total_summ = []

    summarization_recover_api(search_keyword,type)
    regular_summarization, pic_num = get_regular_content(search_keyword,type)
    total_summ.extend(regular_summarization)

    model_summar=[i['abstract_complete'] for i in mongodb_connection('corpus')['summarization'].find({'keyword': search_keyword, 'type': type})][0]
    model_summar_split = model_summar.split('。')
    for item in model_summar_split:
        max_similarity = 0
        word_list = item.split(' ')
        for sentence in regular_summarization:
            index = 0
            for word in word_list:
                if word in sentence:
                    index += 1
            max_similarity = max(max_similarity, index/len(word_list))

        if max_similarity < 0.7:
            total_summ.append(item)
    return total_summ


def resort_summarization(total_summarization,search_keyword,type):
    """
    根据原文重新排序总文摘中的句子
    :param total_summarization: 文摘
    :param path:
    :return: sorted summarization
    """

    full_text=[i['html_test'] for i in mongodb_connection('corpus')['summarization'].find({'keyword':search_keyword,'type':type})][0]
    full_text_sentences = full_text.split('。')

    result = {}
    abstract_index = []
    for sentence in total_summarization:
        max_similarity = 0
        max_index = 0
        for i, item in enumerate(full_text_sentences):
            ratio = difflib.SequenceMatcher(None, sentence, item).ratio()
            max_index = i if ratio > max_similarity else max_index
            max_similarity = max(max_similarity, ratio)
        abstract_index.append(max_index)

        result[max_index] = sentence

    return ''.join([result[k]+'。' for k in sorted(result.keys())])


def summarization_api(search_keyword,type):
    """
    文摘接口
    :param path:数字表示从数据库中获取原始文档，输入为路径表示从存放在这个路径下的html文件中获取原始文档
    :return: str
    """
    search_keyword=unquote(search_keyword)
    type=float(type)
    now=datetime.now()
    if os.path.exists(os.getcwd() + '/pic/{}'.format(search_keyword+'-'+str(type))):
        shutil.rmtree(os.getcwd() + '/pic/{}'.format(search_keyword + '-' + str(type)))
    if mongodb_connection('corpus')['summarization'].count({'keyword':search_keyword,'type':type}) !=0:
        mongodb_connection('corpus')['summarization'].remove({'keyword': search_keyword, 'type': type})
    a = generate_total_summarization(search_keyword,type)
    b = resort_summarization(a,search_keyword,type)
    generate_doc(b,search_keyword,type)
    shutil.rmtree(os.getcwd() + '/pic/{}'.format(search_keyword+'-'+str(type)))
    print("文摘耗时：{}".format(datetime.now()-now))


if __name__ == '__main__':
    summarization_api(sys.argv[1],sys.argv[2])