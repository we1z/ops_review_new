# -*- coding: utf-8 -*
import lda_doc
import numpy as np
from functools import reduce
import copy
import random
import time



def get_doc_vector(probability):
    """
    构造文档向量
    :param probability:每一篇文档的主题分布
    :return: 文档向量
    """
    results = []
    for item in probability:
        result = []
        id_list = [i for i in map(lambda x:x[0], item)]
        k = 0
        for i in range(20):
            if i not in id_list:
                result.append(0)
            else:
                result.append(item[k][1])
                k += 1
        results.append(result)
    return results


def classified_by_euclidean(docs_vector, k):
    """
    使用欧氏距离实现kmeans算法
    :param docs_vector: 句子向量
    :param k: 聚类的类别数量
    :return: 每一个句子的类别编号组成的list
    """
    n_loop = 100
    candidate = []
    core_k = []
    while len(candidate) is not k:
        i = random.randint(0, len(docs_vector)-1)
        if i in candidate:
            continue
        else:
            candidate.append(i)
            core_k.append(docs_vector[i])
    # core_k = docs_vector[:k]
    class_result = {}
    id_result = {}
    for i in range(n_loop):
        # 计算每一个节点同核的距离，将该节点归类到最近的节点中去
        # 定义类容器，存储每一类的文档向量
        for ii in range(k):
            class_result[ii] = []
            id_result[ii] = []
        # 循环所有文档向量
        for jj, item in enumerate(docs_vector):
            dis = {}
            for j, core in enumerate(core_k):
                dis[j] = np.sqrt(np.sum(np.square(np.array(item)-np.array(core))))
            item_class = sorted(dis, key=lambda x: dis[x])
            class_result[item_class[0]].append(item)
            id_result[item_class[0]].append(jj)

        # 保存原有质心向量，如果没有发生更新，跳出循环，返回聚类结果
        old_core = copy.deepcopy(core_k)
        # 更新质心向量
        for item in class_result:
            tmp_data = class_result[item]
            if len(tmp_data) is 0:
                pass
            else:
                core_k[item] = list(map(lambda x:x/len(class_result[item]),reduce(lambda x, y: np.array(x)+np.array(y), tmp_data)))

        if core_k == old_core:
            print('clustering finished at loop:{}'.format(i))
            break

    # print(reduce(lambda x, y: x+y, map(lambda x: len(class_result[x]), class_result)))
    return id_result


def classified_by_cosine(docs_vector, k):
    """
    使用余弦相似度实现kmeans算法
    :param docs_vector: 文档集合
    :param k: 聚类的类别数量
    :return: 每一个句子的类别编号组成的list
    """
    n_loop = 1
    candidate = []
    core_k = []
    while len(candidate) is not k:
        i = random.randint(0, len(docs_vector)-1)
        if i in candidate:
            continue
        else:
            candidate.append(i)
            core_k.append(docs_vector[i])
    # core_k = docs_vector[:k]
    class_result = {}
    id_result = {}
    for i in range(n_loop):
        # 计算每一个节点同核的距离，将该节点归类到最近的节点中去
        # 定义类容器，存储每一类的文档向量
        for ii in range(k):
            class_result[ii] = []
            id_result[ii] = []
        # 循环所有文档向量
        for jj, item in enumerate(docs_vector):
            dis = {}
            for j, core in enumerate(core_k):
                ab = 0
                a2 = 0
                b2 = 0
                for mm in range(len(core)):
                    ab = ab + core[mm]*item[mm]
                    a2 = a2 + core[mm]*core[mm]
                    b2 = b2 + item[mm]*item[mm]
                dis[j] = ab/(np.sqrt(a2)*np.sqrt(b2))
            item_class = sorted(dis, key=lambda x: dis[x])
            class_result[item_class[0]].append(item)
            id_result[item_class[0]].append(jj)

        # 保存原有质心向量，如果没有发生更新，跳出循环，返回聚类结果
        old_core = copy.deepcopy(core_k)
        # 更新质心向量
        for item in class_result:
            tmp_data = class_result[item]
            if len(tmp_data) is 0:
                pass
            else:
                core_k[item] = list(map(lambda x:x/(len(tmp_data)), reduce(lambda x, y: np.array(x)+np.array(y), tmp_data)))

        if core_k == old_core:
            print('clustering finished at loop:{}'.format(i))
            break

    # print(reduce(lambda x, y: x+y, map(lambda x: len(class_result[x]), class_result)))
    return id_result


def get_classified_id(doc, k, method):
    """
    聚类一篇文章的句子，返回聚类结果
    :param doc: 文章
    :param k: 分类的数量
    :param method: 距离计算方法
    :return: 聚类后每一类句子的id
    """
    d = {}
    a = doc.split('。')
    b = lda_doc.get_mult_doc_topic_probaility(a)
    c = get_doc_vector(b)
    if len(c) > k:
        if method == 1:
            d = classified_by_cosine(c, k)
        else:
            d = classified_by_euclidean(c,k)
    else:
        d[0] = []
        for i in range(len(c)):
             d[0].append(i)
    print(d)
    return d


if __name__ == '__main__':
    corpus = conn2db.mongodb_connection('corpus')
    doc = corpus['classification'].find({'$or': [{'type': 1.0}, {'type': 2.0}]}).skip(520).limit(1)
    test_doc = doc[0]['text']

    st1 = time.time()
    d = get_classified_id(test_doc, 3, 2)

    aa = test_doc.split('。')
    print(d)
    for i in d:
        for k in d[i]:
            print(aa[k])
        print('*'*80)
    et1 = time.time()
    print(et1-st1)

