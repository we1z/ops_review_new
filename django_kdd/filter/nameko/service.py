'''
nameko主逻辑代码
'''

from nameko.events import EventDispatcher, event_handler
from nameko.rpc import rpc
import time
import newspaper
from newspaper import Article
from selenium import webdriver
from selenium.common import exceptions
from selenium.webdriver.chrome.options import Options
import datetime
from nameko.timer import timer
from nameko.web.handlers import http
from db_connection import mongodb_connection
import pymongo
from text.calculate import text_classification_api
from text.data_helpers import *
import json
import requests
from lxml import html
import re
from selenium.webdriver.support.wait import WebDriverWait
import selenium.webdriver.support.expected_conditions as EC
from selenium.webdriver.common.by import By
import urllib.parse
from config import *
from spiders import query, query_36kr, restart_driver
import socket
from html import unescape
import os
from tqdm import tqdm



class HttpService:
    '''
    http服务端
    '''

    def __init__(self):
        pass

    name = "http_service"

    dispatch = EventDispatcher()

    @http('POST', '/nameko_dispatch_keyword')
    def dispatching_keyword_method(self, request):
        '''
        接收来自django的bridge的请求（关键词搜索任务），将其下派到爬虫，获取具体网页的链接
        :param request:
        :return:
        '''
        data_json = json.loads(request.get_data(as_text=True))
        keyword = data_json['keyword']
        company = data_json['company']
        if redis_conn.sismember('keyword', keyword):
            logger.info('%s 最近已被搜索过', keyword)
            done_percent = get_finish_percent_method(keyword)
            return 200, json.dumps({"percent": done_percent})

        else:
            # 下派到谷歌爬虫接口和36kr爬虫接口，后去具体网页的链接
            self.dispatch("keyword_36kr_dispatch", (keyword, company))
            self.dispatch('keyword_google_dispatch', (keyword, company))


        return 200, json.dumps({'state': 'ok'})

    @http('POST', '/nameko_dispatch_url')
    def dispatching_url_method(self, request):
        """
        url发布器
        :param request:
        :return:
        """
        data_json = json.loads(request.get_data(as_text=True))
        url_list = data_json['url_list']
        keyword = data_json['keyword']
        company = data_json['company']
        date = datetime.datetime.now()
        prepare_insert_list = []

        # 由于暂时无法读取pdf文件，因此将其都标注为3类，直接显示
        if 'pdf_url_list' in data_json and 'pdf_title_list' in data_json:
            for i in range(len(data_json['pdf_url_list'])):
                prepare_insert_list.append(
                    {'request_time': date, 'state': 'done', 'url': data_json['pdf_url_list'][i], 'text': '', 'type': 3.0
                        , 'keyword': keyword, 'title': data_json['pdf_title_list'][i], 'company': company,
                     'article_html': ''})

        for url in url_list:
            if 'https://books.google.com/' not in url and 'https://www.youtube.com' not in url and not url.endswith('ppt') and not url.endswith('pptx') and not url.endswith('doc') and not url.endswith('docx'):
                prepare_insert_list.append(
                    {"url": url, "keyword": keyword, "request_time": date, 'state': 'not_started', 'company': company})
        if prepare_insert_list:
            try:
                mongodb_connection('corpus')['classification'].insert_many(prepare_insert_list, ordered=False)
            except pymongo.errors.DuplicateKeyError:
                pass
            except pymongo.errors.BulkWriteError:
                pass
        for i in prepare_insert_list:
            if i['state'] == 'not_started':
                self.dispatch("url_dispatch", (i['url'], keyword))

        return 200, json.dumps({'state': 'ok'})

    @http('POST', '/nameko_percent')
    def get_finish_percent_method(self, request):
        """
        获得当前进度
        :param request:
        :return:
        """
        data_json = json.loads(request.get_data(as_text=True))
        keyword = data_json['keyword']
        done_percent = get_finish_percent_method(keyword)

        return 200, json.dumps({'percent': done_percent})

# 原文摘系统任务发布，暂时保留
    # @http('POST', '/dispatch_summary')
    # def dispatch_summary_method(self, request):
    #     data_json = json.loads(request.get_data(as_text=True))
    #     keyword=data_json['keyword']
    #     type=data_json['type']
    #     if type=='market':
    #         self.dispatch('get_summarization',(keyword,1.0))
    #     elif type=='competitor':
    #         self.dispatch('get_summarization',(keyword,2.0))


    @http('POST', '/dispatch_summary')
    def dispatch_summary_method(self, request):
        data_json = json.loads(request.get_data(as_text=True))
        keyword=data_json['keyword']
        type=data_json['type']
        is_regenerate=data_json['is_regenerate']
        if type=='market':
            self.dispatch('get_summarization',(keyword,1.0,is_regenerate))
        elif type=='competitor':
            self.dispatch('get_summarization',(keyword,2.0,is_regenerate))


class ArticleService:
    """ Event listening service. """

    def __init__(self):
        pass

    name = "article_service"

    @event_handler('http_service', 'keyword_36kr_dispatch')
    def kr_search_method(self, args):
        """
        获取相应关键字的36kr链接，然后发送链接给具体网页的爬虫
        :param request:
        :return:
        """

        keyword = args[0]
        company = args[1]

        logger.info('获取 %s 36kr链接', keyword)

        driver = restart_driver(None, False)
        kr_list = query_36kr(keyword, driver)
        driver.close()
        # 只要前三条链接
        kr_list = kr_list[:3]
        if kr_list:
            data = {"keyword": keyword, 'url_list': kr_list[:3], 'company': company}
            requests.post('http://localhost:8006/nameko_dispatch_url', json=data)


    @event_handler('http_service', 'keyword_google_dispatch')
    def google_search_method(self, args):
        """
        获取相应关键字的google链接，然后发送链接给具体网页的爬虫
        :param request:
        :return:
        """
        keyword = args[0]
        company = args[1]

        logger.info('获取 %s 谷歌链接', keyword)
        # 若最近搜索过，则不再搜索
        if redis_conn.sismember('keyword', keyword):
            return
        else:
            redis_conn.sadd('keyword', keyword)

        print('开始google搜索 ' + keyword)

        driver = restart_driver(None, True)

        title_list = []
        url_list = []

        pdf_title_list = []
        pdf_url_list = []

        google_list = query(keyword + " 市场", driver)
        google_list.extend(query(keyword + " 规模", driver))

        for i in google_list:
            if i['title'] in title_list or i['url'] in url_list:
                pass
            else:
                if 'wenku.baidu.com' in i['url'] or i['url'].endswith('.pdf'):
                    if i['url'] in pdf_url_list or i['title'] in pdf_title_list:
                        pass
                    else:
                        pdf_url_list.append(i['url'])
                        pdf_title_list.append(i['title'])
                else:
                    url_list.append(i['url'])
                    title_list.append(i['title'])

        data = {"keyword": keyword, 'url_list': url_list, 'company': company}
        # 为保障速度，分两次发送url
        requests.post('http://localhost:8006/nameko_dispatch_url', json=data)

        google_list = query(keyword + " 全球", driver)
        google_list.extend(query(keyword + " 企业", driver))
        google_list.extend(query(keyword + " 产品", driver))
        google_list.extend(query(keyword + " 创始人", driver))

        title_list = []
        second_url_list = []

        for i in google_list:
            if i['title'] in title_list or i['url'] in url_list:
                pass
            else:
                if 'wenku.baidu.com' in i['url'] or i['url'].endswith('.pdf'):
                    if i['url'] in pdf_url_list or i['title'] in pdf_title_list:
                        pass
                    else:
                        pdf_url_list.append(i['url'])
                        pdf_title_list.append(i['title'])
                else:
                    url_list.append(i['url'])
                    title_list.append(i['title'])
                    second_url_list.append(i['url'])

        driver.close()
        data = {"keyword": keyword, 'url_list': second_url_list, 'pdf_url_list': pdf_url_list,
                'pdf_title_list': pdf_title_list, 'company': company}
        requests.post('http://localhost:8006/nameko_dispatch_url', json=data)

# 原文摘系统，暂时保留
    # @event_handler('http_service', 'get_summarization')
    # def get_summarization(self, args):
    #     """
    #     获取相应关键字的36kr链接，然后发送链接给具体网页的爬虫
    #     :param request:
    #     :return:
    #     """
    #     search_keywod = args[0]
    #     type = args[1]
    #     if os.path.exists('/data/summarization/{}.docx'.format(search_keywod+'-'+str(type))):
    #         logger.info("已经存在 {}-{} 文摘。".format(search_keywod,str(type)))
    #         pass
    #     else:
    #         logger.info("开始生成 {}-{} 文摘，会不会爆炸呢？".format(search_keywod,str(type)))
    #         search_keywod=quote(search_keywod)
    #         os.system('nohup python3 /home/yuanyuan/django_kdd/filter/nameko/get_summarization.py {} {} &'.format(search_keywod,str(type)))
    #         logger.info("search_keywod:{},type:{}".format(search_keywod,str(type)))
    #         pass


    @event_handler('http_service', 'get_summarization')
    def get_summarization(self, args):
        """
        获取相应关键字的36kr链接，然后发送链接给具体网页的爬虫
        :param request:
        :return:
        """
        search_keywod = args[0]
        type = args[1]
        is_regenerate=args[2]
        if is_regenerate=='false':
            if mongodb_connection('corpus')['summarization'].count({'title':search_keywod,'type':type}) !=0:
                logger.info("已经存在 {}-{} 文摘。".format(search_keywod,str(type)))
                pass
            else:
                logger.info("开始生成 {}-{} 文摘，会不会爆炸呢？".format(search_keywod,str(type)))
                get_abstract(search_keywod,type)
                logger.info("search_keywod:{},type:{}".format(search_keywod,str(type)))
                pass
        elif is_regenerate=='true':
            mongodb_connection('corpus')['summarization'].remove({'title': search_keywod, 'type': type})
            logger.info("删除 {}-{} 文摘。".format(search_keywod, str(type)))
            logger.info("开始生成 {}-{} 文摘，会不会爆炸呢？".format(search_keywod, str(type)))
            get_abstract(search_keywod, type)
            logger.info("search_keywod:{},type:{}".format(search_keywod, str(type)))
            pass

        else:
            pass



    @event_handler("http_service", "url_dispatch")
    def get_url_detail(self, args):
        """
        获取url的具体文章内容，具体网页的爬虫
        :param args:
        :return:
        """
        url = args[0]
        keyword = args[1]
        logger.info("receive url task: %s", url)
        # 因为newspaper库不能解析36r网址，所以独立处理
        if '//36kr.com/p/' in url and url.endswith('.html'):
            r = requests.get(url)
            assert r.status_code == 200
            # print(r.text)
            root = html.fromstring(r.text)
            a = root.xpath('//script')

            for i in a:
                if i.text and i.text.startswith('var props='):
                    j = i.text[10:]
                    try:
                        j = json.loads(j)
                    except json.decoder.JSONDecodeError as e:
                        pos_index = e.pos
                        j = json.loads(j[:pos_index])
                        article_html = unescape(j['detailArticle|post']['content'])
                        root = html.fromstring(article_html)
                        content = root.xpath('string(.)')
                        title = j['detailArticle|post']['title']

                        if len(re.findall(keyword, content)) < 4:
                            mongodb_connection('corpus')['classification'].update({"keyword": keyword, "url": url}, {
                                '$set': {"done_time": datetime.datetime.now(), "text": content, "title": title,
                                         "state": "done", 'type': 0.0}})
                        else:
                            mongodb_connection('corpus')['classification'].update({"keyword": keyword, "url": url}, {
                                '$set': {"done_time": datetime.datetime.now(), "text": content, "title": title,
                                         "state": "not_classified", 'article_html': article_html}})

                        logger.info("%s done", url)
                        break
        else:
            article = Article(url=url, language='zh', keep_article_html=True, request_timeout=10)
            article.download()
            try:
                if article.download_state != 2:
                    print(url, '1')
                    article = None
                else:
                    article.parse()
            except newspaper.article.ArticleException:
                article = None

            if not article:
                article = Article(url=url, language='zh', keep_article_html=True, request_timeout=15,
                                  proxies={'http': 'socks5://127.0.0.1:' + socks5_port[current_socks5_port_index],
                                           'https': 'socks5://127.0.0.1:' + socks5_port[current_socks5_port_index]})
                article.download()
                try:
                    if article.download_state != 2:
                        print(url, '2')
                        article = None
                    else:
                        article.parse()
                except newspaper.article.ArticleException:
                    article = None

            if not article:
                print('go chrome')
                driver = restart_driver(None, True)
                print('go chrome 1')
                try:
                    driver.get(url)
                    print('go chrome 2')
                    article = Article(url=url, language='zh', keep_article_html=True, request_timeout=5)
                    article.download(input_html=driver.page_source)
                    if article.download_state != 2:
                        print(url, article.download_state)
                        article = None
                    else:
                        article.parse()
                except exceptions.TimeoutException:
                    pass
                except exceptions.WebDriverException:
                    pass
                except socket.timeout:
                    pass
                finally:
                    driver.close()

            if article and article.text:
                print(url,'ready success')
                print(str({"keyword": keyword, "url": url}),str({
                    '$set': {"done_time": datetime.datetime.now(), "text": article.text, "title": article.title,
                             "state": "not_classified", 'article_html': article.article_html}}))
                mongodb_connection('corpus')['classification'].update_one({"keyword": keyword, "url": url}, {
                    '$set': {"done_time": datetime.datetime.now(), "text": article.text, "title": article.title,
                             "state": "not_classified", 'article_html': article.article_html}})
                print(url,'success')
            else:
                print(url,'ready fail')
                mongodb_connection('corpus')['classification'].update_one({"keyword": keyword, "url": url},
                                                                      {'$set': {'state': "fail"}})
                print(url,'fail')

            logger.info("%s done", url)

    @timer(interval=9)
    def deal_classify(self):
        """
        定期对已经爬取的文本进行深度学习的分类
        :return:
        """
        un_deal_list = [{'text': i['text'], '_id': i['_id']} for i in
                        mongodb_connection('corpus')['classification'].find({'state': 'not_classified'})]
        if un_deal_list:
            classification_result = text_classification_api([i['text'] for i in un_deal_list])[0]
            for i in range(0, len(classification_result)):
                if classification_result[i] != 0.0:
                    mongodb_connection('corpus')['classification'].update({'_id': un_deal_list[i]['_id']}, {'$set':
                                                                                                                {
                                                                                                                    'state': "not_written",
                                                                                                                    "type":
                                                                                                                        classification_result[
                                                                                                                            i]}})
                else:
                    mongodb_connection('corpus')['classification'].update({'_id': un_deal_list[i]['_id']}, {'$set':
                                                                                                                {
                                                                                                                    'state': "done",
                                                                                                                    "type": 0.0}})

            logger.info('classify done')

    @timer(interval=900)
    def clear_redis_keyword(self):
        """
        以防出错，定期删除redis内容
        :return:
        """
        redis_conn.spop('keyword')

    @timer(interval=3600 * 5)
    def clear_chrome_and_chromedriver(self):
        """
        由于浏览器的bug，chromedriver会在退出不能关闭，造成内存占用
        :return:
        """
        if 1 < datetime.datetime.now().hour < 7:
            os.system('killall chrome')
            os.system('killall chromedriver')

    # @timer(interval=13)
    # def deal_write(self):
    #     """
    #     定期写html文件
    #     :return:
    #     """
    #     un_write_list = [i for i in
    #                      mongodb_connection('corpus')['classification'].find({'state': 'not_written','keyword': {'$exists':1}})]
    #     un_write_keyword_set = set([i['keyword'] for i in un_write_list])
    #     for keyword in un_write_keyword_set:
    #         unfinished_keyword_list = [i for i in
    #                                    mongodb_connection('corpus')['classification'].find({'keyword': keyword})]
    #         write_text_list = [i['text'] for i in unfinished_keyword_list if i['state'] == 'done' and i['state'] != 0.0]
    #
    #         market_html_write_content = []
    #         competitor_html_write_content = []
    #
    #         for i in [j for j in unfinished_keyword_list if j['state'] == 'done' and j['type'] == 1.0]:
    #             market_html_write_content.append({
    #                 "title": i['title'],
    #                 "text": i['article_html'],
    #                 "url": i['url'],
    #                 'type': 1.0
    #             })
    #
    #         for i in [j for j in unfinished_keyword_list if j['state'] == 'done' and j['type'] == 3.0]:
    #             market_html_write_content.append({
    #                 "title": i['title'],
    #                 "url": i['url'],
    #                 "type": 3.0
    #             })
    #
    #         for i in [j for j in unfinished_keyword_list if j['state'] == 'done' and j['type'] == 2.0]:
    #             competitor_html_write_content.append({
    #                 "title": i['title'],
    #                 "text": i['article_html'],
    #                 "url": i['url'],
    #                 "type": 2.0
    #             })
    #
    #         for i in [j for j in unfinished_keyword_list if j['state'] == 'not_written' and j['type'] == 1.0]:
    #             if is_similar_text(write_text_list, i['text']):
    #                 mongodb_connection('corpus')['classification'].update({'_id': i['_id']},
    #                                                                       {'$set': {'state': 'duplicate'}})
    #             else:
    #                 market_html_write_content.append({
    #                     "title": i['title'],
    #                     "text": i['article_html'],
    #                     "url": i['url'],
    #                     'type': 1.0
    #                 })
    #                 mongodb_connection('corpus')['classification'].update({'_id': i['_id']},
    #                                                                       {'$set': {'state': 'done'}})
    #                 write_text_list.append(i['text'])
    #
    #         for i in [j for j in unfinished_keyword_list if j['state'] == 'not_written' and j['type'] == 2.0]:
    #             if is_similar_text(write_text_list, i['text']):
    #                 mongodb_connection('corpus')['classification'].update({'_id': i['_id']},
    #                                                                       {'$set': {'state': 'duplicate'}})
    #             else:
    #                 competitor_html_write_content.append({
    #                     "title": i['title'],
    #                     "text": i['article_html'],
    #                     "url": i['url'],
    #                     'type': 2.0
    #                 })
    #                 mongodb_connection('corpus')['classification'].update({'_id': i['_id']},
    #                                                                       {'$set': {'state': 'done'}})
    #                 write_text_list.append(i['text'])
    #         keyword_state = get_finish_percent_method(keyword)
    #         write_html_file(market_html_write_content, 1.0, keyword, keyword_state)
    #         write_html_file(competitor_html_write_content, 2.0, keyword, keyword_state)
    #         logger.info('%s written ' + str(keyword_state), keyword)





def get_finish_percent_method(keyword):
    """
    获取进度
    :param keyword:
    :return:
    """
    try:
        done_percent = mongodb_connection('corpus')['classification'].count(
            {'keyword': keyword, '$or': [{'state': 'done'}, {'state': 'fail'}]}) / \
                       mongodb_connection('corpus')['classification'].count({'keyword': keyword})
        return done_percent
    except ZeroDivisionError:
        logger.info("获取进度，发生除0错误。")
        return 0.0

def get_abstract(keyword,type):
    # 获取摘要
    if mongodb_connection('corpus')['summarization'].count({'keyword':keyword,'type':type}) ==0:
        final=[]
        text_list=[unescape(i['text']).split('\n') for i in mongodb_connection('corpus')['classification'].find({'keyword':keyword,'type':type})]

        total=[]
        for i in text_list:
            total.extend(i)
        total=list(set(total))

        mission_list = [total[i:i + 100] for i in range(0, len(total), 100)]
        for i in tqdm(mission_list):
            classification_result=text_classification_api(i)[0]
            logger.info(classification_result)
            for j in range(0, len(classification_result)):
                if classification_result[j] == 1.0:
                    final.append(i[j])

        final=[i.strip() for i in final if i !='']

        zip=len(final)/len(total)
        logger.info(zip)

        mongodb_connection('corpus')['summarization'].insert({'title':keyword,'type':type,'text':final,'zip':zip})
        pass
    pass