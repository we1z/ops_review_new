from newspaper import Article
from selenium import webdriver
from selenium.common import exceptions
from selenium.webdriver.chrome.options import Options
import urllib.parse
import time
from selenium.webdriver.support.wait import WebDriverWait
import selenium.webdriver.support.expected_conditions as EC
from selenium.webdriver.common.by import By
import logging
from lxml import html
from config import *
import datetime


def query(word, driver):
    """
    google搜索入口
    :param driver:
    :param word:搜索关键字
    :return: 百度搜索结果列表
    """
    url = r'https://www.google.com.hk/search?q='
    words = urllib.parse.quote(word)
    response_data = ''

    try:

        driver.get(url + words)
        response_data = driver.page_source

        # 搜索第二页内容
        driver.execute_script('window.scrollTo(0, document.body.scrollHeight)')
        driver.find_element_by_xpath('//a[@aria-label="Page 2"]').click()
        time.sleep(0.4)
        if response_data!=driver.page_source:
            response_data = response_data + driver.page_source
        else:
            time.sleep(0.4)
            response_data = response_data + driver.page_source
        # 搜第三页内容
        # r = 0
        # while r == 0:
        #     driver.execute_script('window.scrollTo(0, document.body.scrollHeight)')
        #     time.sleep(0.4)
        #     r = driver.execute_script('return document.body.scrollTop')
        #     try:
        #         driver.find_element_by_xpath('//div[@id="page"]/a[last()]').click()
        #     except exceptions.WebDriverException:
        #
        #         continue
        #     break
        #
        # response_data = response_data + driver.page_source

    except exceptions.NoSuchElementException:
        logger.error('%s google无搜索内容', word)
        driver.save_screenshot('/home/yuanyuan/google_spider_debug/{}.png'.format(datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')))
        pass
    except (exceptions.StaleElementReferenceException, exceptions.TimeoutException):
        logger.error('%s google搜索超时',word)
        global current_socks5_port_index
        current_socks5_port_index=(current_socks5_port_index+1)%len(socks5_port)
        logger.info('更换sock5端口至 %s', socks5_port[current_socks5_port_index])
        driver=restart_driver(driver,True)
        return query(word, driver)
    except ConnectionRefusedError:
        logger.error('ConnectionRefusedError错误')
        driver = restart_driver(driver, True)
        return query(word, driver)

    result = []
    root = html.fromstring(response_data)
    list = root.xpath('//div[@class="g"]')

    for i in list:
        item = {}
        try:
            item['url'] = i.xpath('./div/div/h3/a/@href')[0]

            item['title'] = i.xpath('./div/div/h3/a')[0].xpath('string(.)')
            item['summary'] = i.xpath('./div/div/div/div/span')[0].xpath('string(.)')
            result.append(item)
        except IndexError:
            pass

    return result


def query_36kr(keyword, driver2):
    """
    36kr搜索入口
    :param keyword: 关键词
    :param driver2:
    :return:
    """
    url = 'http://36kr.com/search/articles/' + keyword

    try:
        driver2.get(url)
    except exceptions.TimeoutException:
        pass

    try:
        contain_element = WebDriverWait(driver2, 10).until(
            EC.presence_of_element_located((By.XPATH, '//div[@class="kr_article_list"]|//div[@class="no-result-box"]')))
    except exceptions.TimeoutException:
        print('36kr失败')
        return []

    if contain_element.text.startswith('很抱歉，没有找到'):
        return []
    else:
        root = html.fromstring(driver2.page_source)
        list = root.xpath('//div[@class="kr_article_list"]/div/ul/li/a')
        return ['http://36kr.com' + i.attrib['href'] for i in list]


def restart_driver(driver, is_sock5):
    """
    返回chrome driver
    :param driver: 原driver
    :param is_sock5: 是否使用sock5代理
    :return:
    """
    try:
        if driver:
            driver.quit()
    except:
        pass
    options = Options()
    options.add_argument("--headless")
    if is_sock5:
        options.add_argument("--proxy-server=socks5://" + '127.0.0.1' + ":" + socks5_port[current_socks5_port_index])
    prefs = {"profile.managed_default_content_settings.images": 2}
    options.add_experimental_option("prefs", prefs)
    driver = webdriver.Chrome(chrome_options=options)
    driver.set_page_load_timeout(20)
    return driver

