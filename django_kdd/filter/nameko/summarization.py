# -*- coding: utf-8 -*
import os
import sys
import pickle
from db_connection import mongodb_connection
from config import *
import random
import numpy as np
import tensorflow as tf
tf.reset_default_graph()


# We use a number of buckets for sampling
_buckets = [(2000, 100)]
emb_init = tf.truncated_normal_initializer(mean=0.0, stddev=0.01)
fc_layer = tf.contrib.layers.fully_connected



def load_dict(dict_path, max_vocab=None):
    """
    载入词典
    :param dict_path: 词典存放路径
    :param max_vocab: 词典的最大数目
    :return: 词典
    """
    logger.info("Try load dict from {}.".format(dict_path))
    try:
        dict_file = open(dict_path, encoding='utf-8')
        dict_data = dict_file.readlines()
        dict_file.close()
    except:
        logger.info(
            "Load dict {dict} failed, create later.".format(dict=dict_path))
        return None

    dict_data = list(map(lambda x: x.split(), dict_data))
    if max_vocab:
        dict_data = list(filter(lambda x: int(x[0]) < max_vocab, dict_data))
    tok2id = dict(map(lambda x: (x[1], int(x[0])), dict_data))
    id2tok = dict(map(lambda x: (int(x[0]), x[1]), dict_data))
    logger.info(
        "Load dict {} with {} words.".format(dict_path, len(tok2id)))
    return (tok2id, id2tok)



def create_counter(dict_path, corpus, counter, max_vocab=None):
    """
    统计语料库中各个词汇的出现次数
    :param dict_path: 用于日志，显示语料位置
    :param corpus:语料
    :param counter:计数器
    :param max_vocab:最大词汇量
    :return:
    """

    logger.info("Create dict {}.".format(dict_path))
    if not counter:
        counter = {}
    for line in corpus:
        for word in line:
            try:
                counter[word] += 1
            except:
                counter[word] = 1
    return counter



def create_dict(dict_path, counter, max_vocab):
    """
    创建字典，统计词频，从大到小排序，取前max_vocab个词汇，编上id，保存为字典
    :param dict_path: 保存位置
    :param counter: 计数器
    :param max_vocab: 最大词汇数
    :return: 词典
    """
    for mark_t in MARKS:
        if mark_t in counter:
            del counter[mark_t]
            logger.warning("{} appears in corpus.".format(mark_t))

    counter = list(counter.items())
    counter.sort(key=lambda x: -x[1])

    words = list(map(lambda x: x[0], counter))
    words = [MARK_PAD, MARK_UNK1, MARK_UNK2, MARK_UNK3, MARK_UNK4, MARK_UNK5, MARK_UNK6, MARK_EOS, MARK_GO] + words
    if max_vocab:
        words = words[:max_vocab]

    tok2id = dict()
    id2tok = dict()
    with open(dict_path, 'w', encoding='utf-8') as dict_file:
        for idx, tok in enumerate(words):
            # print(idx, tok, file=dict_file)
            dict_file.write(str(idx) + ' ' + tok)
            dict_file.write('\n')
            tok2id[tok] = idx
            id2tok[idx] = tok

    logger.info(
        "Create dict {} with {} words.".format(dict_path, len(words)))
    return (tok2id, id2tok)



def corpus_map2id(data, tok2id):
    """
    将语料库中的词汇转化为id，特殊字符特殊处理
    :param data: 语料
    :param tok2id: 词到id的映射
    :return: id
    """
    ret = []
    unk = 0
    tot = 0
    for doc in data:
        tmp = []
        for word in doc:
            tot += 1
            try:
                tmp.append(tok2id[word])
            except:
                if len(word) == 1:
                    tmp.append(ID_UNK1)
                elif len(word) == 2:
                    tmp.append(ID_UNK2)
                elif len(word) == 3:
                    tmp.append(ID_UNK3)
                elif len(word) == 4:
                    tmp.append(ID_UNK4)
                elif len(word) == 5:
                    tmp.append(ID_UNK5)
                elif len(word) > 5:
                    tmp.append(ID_UNK6)
                unk += 1
        ret.append(tmp)
    return ret, (tot - unk) / tot



def sen_map2tok(sen, id2tok):
    """
    id到词的转化
    :param sen:文档的数字表示，list
    :param id2tok: id到语料的映射
    :return: sen表示的文档真实内容
    """
    return list(map(lambda x: id2tok[x], sen))



def min_counter(dict):
    """
    控制词典的大小，去除其中词频为1的词汇
    :param dict:
    :return:
    """
    return {k: v for k, v in dict.items() if v > 1}



def load_data(database_list, valid_size, doc_dict_path, sum_dict_path, max_doc_vocab=None, max_sum_vocab=None):
    """
    加载训练数据
    :param database_list: 训练文档的id
    :param valid_size: 验证集的数量
    :param doc_dict_path: 全文字典路径
    :param sum_dict_path: 摘要字典路径
    :param max_doc_vocab: 全文字典最大容量
    :param max_sum_vocab: 摘要字典最大容量
    :return: 文档的数字表示集，词典
    """
    logger.info(
        "Load document num:{}.".format(
            len(database_list)))

    corpus = mongodb_connection('corpus')

    col = corpus['abstract']

    content_counter = {}
    abstract_counter = {}

    doc_dict = load_dict(doc_dict_path, max_doc_vocab)

    # 如果没有词典，则从数据库中提取所有文章，建立词典，分别是全文词典和摘要词典
    pairs_count = len(database_list)
    batch_size = pairs_count//10 if pairs_count//10 != 0 else 1
    if not doc_dict:
        for count in range(0, pairs_count, batch_size):
            docs = []
            sums = []
            start = count if count != 0 else valid_size+count  # 前valid_size个数据用于验证
            end = count+batch_size if count+batch_size <= pairs_count else pairs_count   # 防止数组溢出
            for i in database_list[start:end]:
                data = col.find({'_id': i})
                docs.append(data['content_list'])
                sums.append(data['abstract_list'])

            assert len(docs) == len(sums)
            logger.info("Load {num} pairs of data.".format(num=len(docs)))

            content_counter = create_counter(doc_dict_path, docs, content_counter, max_doc_vocab)

            if len(content_counter) > 700000:
                content_counter = min_counter(content_counter)

            abstract_counter = create_counter(sum_dict_path, sums, abstract_counter, max_sum_vocab)
            if len(abstract_counter) > 700000:
                abstract_counter = min_counter(abstract_counter)

        content_counter = min_counter(content_counter)
        abstract_counter = min_counter(abstract_counter)

        doc_dict = create_dict(doc_dict_path, content_counter, max_doc_vocab)
        sum_dict = create_dict(sum_dict_path, abstract_counter, max_sum_vocab)

    else:
        doc_dict = load_dict(doc_dict_path, max_doc_vocab)
        sum_dict = load_dict(sum_dict_path, max_sum_vocab)

    docid = []
    sumid = []

    if os.path.exists('docid.pkl!!') and os.path.exists('sumid.pkl!!'):
        with open('sumid.pkl', mode='rb') as f:
            sumid = pickle.load(f)
        with open('docid.pkl', mode='rb') as f:
            docid = pickle.load(f)
    else:
        for count in range(0, pairs_count, batch_size):
            docs = []
            sums = []
            start = count if count != 0 else valid_size+count  # 前valid_size个数据用于验证
            end = count+batch_size if count+batch_size <= pairs_count else pairs_count   # 防止数组溢出
            for i in database_list[start:end]:
                data = [j for j in col.find({'_id': i})][0]
                docs.append(data['content_list'])
                sums.append(data['abstract_list'])

            docid_tmp, cover = corpus_map2id(docs, doc_dict[0])
            logger.info(
                "Doc dict covers {:.2f}% words.".format(cover * 100))
            docid = docid + docid_tmp

            sumid_tmp, cover = corpus_map2id(sums, sum_dict[0])
            logger.info(
                "Sum dict covers {:.2f}% words.".format(cover * 100))
            sumid = sumid + sumid_tmp

        docid_file = open('docid.pkl', 'wb')
        pickle.dump(docid, docid_file)
        docid_file.close()
        sumid_file = open('sumid.pkl', 'wb')
        pickle.dump(sumid, sumid_file)
        sumid_file.close()

    return docid, sumid, doc_dict, sum_dict



def load_valid_data(database_list, valid_size, doc_dict, sum_dict):
    """
    加载验证文档集，显示词典转化率
    :param doc_filename:
    :param sum_filename:
    :param doc_dict:
    :param sum_dict:
    :return:
    """
    logger.info(
        "Load validation document num {}.".format(valid_size))
    docs = []
    sums = []
    abstract = mongodb_connection('corpus')

    for i in range(valid_size):
        data = [j for j in abstract['abstract'].find({'_id': database_list[i]})][0]
        docs.append(data['content_list'])
        sums.append(data['abstract_list'])
    logger.info("Load {} validation documents.".format(len(docs)))

    docid, cover = corpus_map2id(docs, doc_dict[0])
    logger.info(
        "Doc dict covers {:.2f}% words on validation set.".format(cover * 100))
    sumid, cover = corpus_map2id(sums, sum_dict[0])
    logger.info(
        "Sum dict covers {:.2f}% words on validation set.".format(cover * 100))
    return docid, sumid



def corpus_preprocess(corpus):
    """
    文档预处理，将数字都转化为’#‘
    :param corpus: 语料
    :return:
    """
    import re
    ret = []
    for line in corpus:
        x = re.sub('\\d', '#', line)
        ret.append(x)
    return ret




def load_test_data(search_keyword,type, doc_dict):
    """
    加载测试文档集，转化其中的词为id
    :param doc_filename:
    :param doc_dict:
    :return:
    """

    # with open(doc_filename, encoding='utf-8') as docfile:
    #     docs = docfile.readlines()
    docs=''.join([i['processed_txt'] for i in mongodb_connection('corpus')['summarization'].find({'keyword': search_keyword, 'type': type})]).split('\n')
    docs=docs[:-1]

    docs = corpus_preprocess(docs)

    logger.info("Load {num} testing documents.".format(num=len(docs)))
    docs = list(map(lambda x: x.split(), docs))

    docid, cover = corpus_map2id(docs, doc_dict[0])
    logger.info(
        "Doc dict covers {:.2f}% words.".format(cover * 100))

    return docid



def create_model(session, forward_only):
    """Create model and initialize or load parameters in session."""
    dtype = tf.float32
    model = BiGRUModel(
        doc_vocab_size,
        sum_vocab_size,
        _buckets,
        size,
        num_layers,
        embsize,
        max_gradient,
        batch_size,
        learning_rate,
        forward_only=forward_only,
        dtype=dtype)
    if checkpoint != "":
        # print(checkpoint)
        # ckpt = checkpoint
        ckpt = tf.train.get_checkpoint_state(checkpoint)
        if ckpt:
            ckpt = ckpt.model_checkpoint_path
    else:
        ckpt = tf.train.get_checkpoint_state(train_dir)
        if ckpt:
            ckpt = ckpt.model_checkpoint_path
    if ckpt and tf.train.checkpoint_exists(ckpt):
        logger.info("Reading model parameters from %s" % ckpt)
        model.saver.restore(session, ckpt)
    else:
        logger.info("Created model with fresh parameters.")
        session.run(tf.global_variables_initializer())
    return model



def decode_1(search_keyword,type):
    # Load vocabularies.

    doc_dict = load_dict(data_dir + "/doc_dict.txt")
    sum_dict = load_dict(data_dir + "/sum_dict.txt")
    if doc_dict is None or sum_dict is None:
        logger.warning("Dict not found.")
    # data = load_test_data('data/{}.test.3.txt'.format(filename), doc_dict)
    data=load_test_data(search_keyword,type, doc_dict)

    with tf.Session() as sess:
        # Create model and load parameters.
        logger.info("Creating %d layers of %d units." %
                     (num_layers, size))
        model = create_model(sess, True)

        result = []
        for idx, token_ids in enumerate(data):

            # Get a 1-element batch to feed the sentence to the model.
            encoder_inputs, decoder_inputs, encoder_len, decoder_len = \
                    model.get_batch(
                        {0: [(token_ids, [ID_GO, ID_EOS])]}, 0)
            if batch_size == 1 and geneos:
                    loss, outputs = model.step(sess, encoder_inputs, decoder_inputs, encoder_len, decoder_len, True)
                    outputs = [np.argmax(item) for item in outputs[0]]
            else:
                outputs = model.step_beam( sess, encoder_inputs, encoder_len, geneos=geneos)

            # If there is an EOS symbol in outputs, cut them at that point.
            if ID_EOS in outputs:
                outputs = outputs[:outputs.index(ID_EOS)]
            vvv = sen_map2tok(outputs, sum_dict[1])

            gen_sum = ' '.join(vvv)
            result.append(gen_sum)
            logger.info("Finish {} samples. :: {}".format(idx, gen_sum))

        output=''
        for item in result:
            output+=item+'\n'
        mongodb_connection('corpus')['summarization'].update({'keyword':search_keyword,'type':type},{'$set':{'output':output}})



class BiGRUModel(object):
    def __init__(self,
                 source_vocab_size,
                 target_vocab_size,
                 buckets,
                 state_size,
                 num_layers,
                 embedding_size,
                 max_gradient,
                 batch_size,
                 learning_rate,
                 forward_only=False,
                 dtype=tf.float32):

        self.source_vocab_size = source_vocab_size
        self.target_vocab_size = target_vocab_size
        self.buckets = buckets
        self.batch_size = batch_size
        self.learning_rate = learning_rate
        self.global_step = tf.Variable(0, trainable=False, name="global_step")
        self.state_size = state_size

        self.encoder_inputs = tf.placeholder(
            tf.int32, shape=[self.batch_size, None])
        self.decoder_inputs = tf.placeholder(
            tf.int32, shape=[self.batch_size, None])
        self.decoder_targets = tf.placeholder(
            tf.int32, shape=[self.batch_size, None])
        self.encoder_len = tf.placeholder(tf.int32, shape=[self.batch_size])
        self.decoder_len = tf.placeholder(tf.int32, shape=[self.batch_size])
        self.beam_tok = tf.placeholder(tf.int32, shape=[self.batch_size])
        self.prev_att = tf.placeholder(
            tf.float32, shape=[self.batch_size, state_size * 2])

        encoder_fw_cell = tf.contrib.rnn.GRUCell(state_size)
        encoder_bw_cell = tf.contrib.rnn.GRUCell(state_size)
        decoder_cell = tf.contrib.rnn.GRUCell(state_size)

        if not forward_only:
            encoder_fw_cell = tf.contrib.rnn.DropoutWrapper(
                encoder_fw_cell, output_keep_prob=0.50)
            encoder_bw_cell = tf.contrib.rnn.DropoutWrapper(
                encoder_bw_cell, output_keep_prob=0.50)
            decoder_cell = tf.contrib.rnn.DropoutWrapper(
                decoder_cell, output_keep_prob=0.50)

        with tf.variable_scope("seq2seq", dtype=dtype):
            with tf.variable_scope("encoder"):
                ###
                try:
                   encoder_emb = tf.get_variable("embedding", [source_vocab_size, embedding_size], initializer=emb_init)
                except:
                    pass
                encoder_inputs_emb = tf.nn.embedding_lookup(
                    encoder_emb, self.encoder_inputs)

                encoder_outputs, encoder_states = \
                    tf.nn.bidirectional_dynamic_rnn(
                        encoder_fw_cell, encoder_bw_cell, encoder_inputs_emb,
                        sequence_length=self.encoder_len, dtype=dtype)

            with tf.variable_scope("init_state"):
                init_state = fc_layer(
                    tf.concat(encoder_states, 1), state_size)
                # the shape of bidirectional_dynamic_rnn is weird
                # None for batch_size
                self.init_state = init_state
                self.init_state.set_shape([self.batch_size, state_size])
                self.att_states = tf.concat(encoder_outputs, 2)
                self.att_states.set_shape([self.batch_size, None, state_size * 2])

            with tf.variable_scope("attention"):
                attention = tf.contrib.seq2seq.BahdanauAttention(
                    state_size, self.att_states, self.encoder_len)
                decoder_cell = tf.contrib.seq2seq.DynamicAttentionWrapper(
                    decoder_cell, attention, state_size * 2)
                wrapper_state = tf.contrib.seq2seq.DynamicAttentionWrapperState(
                    self.init_state, self.prev_att)

            with tf.variable_scope("decoder") as scope:

                decoder_emb = tf.get_variable(
                    "embedding", [target_vocab_size, embedding_size],
                    initializer=emb_init)

                decoder_cell = tf.contrib.rnn.OutputProjectionWrapper(
                    decoder_cell, target_vocab_size)

                if not forward_only:
                    decoder_inputs_emb = tf.nn.embedding_lookup(
                        decoder_emb, self.decoder_inputs)

                    helper = tf.contrib.seq2seq.TrainingHelper(
                        decoder_inputs_emb, self.decoder_len)
                    decoder = tf.contrib.seq2seq.BasicDecoder(
                        decoder_cell, helper, wrapper_state)

                    outputs, final_state = \
                        tf.contrib.seq2seq.dynamic_decode(decoder)

                    outputs_logits = outputs[0]
                    self.outputs = outputs_logits

                    weights = tf.sequence_mask(
                        self.decoder_len, dtype=tf.float32)

                    loss_t = tf.contrib.seq2seq.sequence_loss(
                        outputs_logits, self.decoder_targets, weights,
                        average_across_timesteps=False,
                        average_across_batch=False)
                    self.loss = tf.reduce_sum(loss_t) / self.batch_size

                    params = tf.trainable_variables()
                    opt = tf.train.AdadeltaOptimizer(
                        self.learning_rate, epsilon=1e-6)
                    gradients = tf.gradients(self.loss, params)
                    clipped_gradients, norm = \
                        tf.clip_by_global_norm(gradients, max_gradient)
                    self.updates = opt.apply_gradients(
                        zip(clipped_gradients, params),
                        global_step=self.global_step)

                    tf.summary.scalar('loss', self.loss)
                else:
                    self.loss = tf.constant(0)
                    with tf.variable_scope("proj") as scope:
                        output_fn = lambda x: fc_layer(
                            x, target_vocab_size, scope=scope)

                    st_toks = tf.convert_to_tensor(
                        [ID_GO] * batch_size, dtype=tf.int32)

                    helper = tf.contrib.seq2seq.GreedyEmbeddingHelper(
                        decoder_emb, st_toks, ID_EOS)

                    decoder = tf.contrib.seq2seq.BasicDecoder(
                        decoder_cell, helper, wrapper_state)

                    outputs, final_state = \
                        tf.contrib.seq2seq.dynamic_decode(decoder)

                    self.outputs = outputs[0]

                    # single step decode for beam search
                    with tf.variable_scope("decoder", reuse=True):
                        beam_emb = tf.nn.embedding_lookup(
                            decoder_emb, self.beam_tok)
                        self.beam_outputs, self.beam_nxt_state, _, _ = \
                            decoder.step(0, beam_emb, wrapper_state)
                        self.beam_logsoftmax = \
                            tf.nn.log_softmax(self.beam_outputs[0])

        self.saver = tf.train.Saver(tf.global_variables(), max_to_keep=0)
        self.summary_merge = tf.summary.merge_all()

    def step(self,
             session,
             encoder_inputs,
             decoder_inputs,
             encoder_len,
             decoder_len,
             forward_only,
             summary_writer=None):

        # dim fit is important for sequence_mask
        # TODO better way to use sequence_mask
        if encoder_inputs.shape[1] != max(encoder_len):
            raise ValueError("encoder_inputs and encoder_len does not fit")
        if not forward_only and \
                        decoder_inputs.shape[1] != max(decoder_len) + 1:
            raise ValueError("decoder_inputs and decoder_len does not fit")
        input_feed = {}
        input_feed[self.encoder_inputs] = encoder_inputs
        input_feed[self.decoder_inputs] = decoder_inputs[:, :-1]
        input_feed[self.decoder_targets] = decoder_inputs[:, 1:]
        input_feed[self.encoder_len] = encoder_len
        input_feed[self.decoder_len] = decoder_len
        input_feed[self.prev_att] = np.zeros(
            [self.batch_size, 2 * self.state_size])

        if forward_only:
            output_feed = [self.loss, self.outputs]
        else:
            output_feed = [self.loss, self.updates]

        if summary_writer:
            output_feed += [self.summary_merge, self.global_step]

        outputs = session.run(output_feed, input_feed)

        if summary_writer:
            summary_writer.add_summary(outputs[2], outputs[3])
        return outputs[:2]

    def step_beam(self,
                  session,
                  encoder_inputs,
                  encoder_len,
                  max_len=1000,
                  geneos=True):

        beam_size = self.batch_size

        if encoder_inputs.shape[0] == 1:
            encoder_inputs = np.repeat(encoder_inputs, beam_size, axis=0)
            encoder_len = np.repeat(encoder_len, beam_size, axis=0)

        if encoder_inputs.shape[1] != max(encoder_len):
            raise ValueError("encoder_inputs and encoder_len does not fit")
        # generate attention_states
        input_feed = {}
        input_feed[self.encoder_inputs] = encoder_inputs
        input_feed[self.encoder_len] = encoder_len
        output_feed = [self.att_states, self.init_state]
        outputs = session.run(output_feed, input_feed)

        att_states = outputs[0]
        prev_state = outputs[1]
        prev_tok = np.ones([beam_size], dtype="int32") * ID_GO
        prev_att = np.zeros([self.batch_size, 2 * self.state_size])

        input_feed = {}
        input_feed[self.att_states] = att_states
        input_feed[self.encoder_len] = encoder_len

        ret = [[]] * beam_size
        neos = np.ones([beam_size], dtype="bool")

        score = np.ones([beam_size], dtype="float32") * (-1e8)
        score[0] = 0

        beam_att = np.zeros(
            [self.batch_size, self.state_size * 2], dtype="float32")

        for i in range(max_len):
            input_feed[self.init_state] = prev_state
            input_feed[self.beam_tok] = prev_tok
            input_feed[self.prev_att] = beam_att
            output_feed = [self.beam_nxt_state[1],
                           self.beam_logsoftmax,
                           self.beam_nxt_state[0]]

            outputs = session.run(output_feed, input_feed)

            beam_att = outputs[0]
            tok_logsoftmax = np.asarray(outputs[1])
            tok_logsoftmax = tok_logsoftmax.reshape(
                [beam_size, self.target_vocab_size])
            if not geneos:
                tok_logsoftmax[:, ID_EOS] = -1e8

            tok_argsort = np.argsort(tok_logsoftmax, axis=1)[:, -beam_size:]
            tmp_arg0 = np.arange(beam_size).reshape([beam_size, 1])
            tok_argsort_score = tok_logsoftmax[tmp_arg0, tok_argsort]
            tok_argsort_score *= neos.reshape([beam_size, 1])
            tok_argsort_score += score.reshape([beam_size, 1])
            all_arg = np.argsort(tok_argsort_score.flatten())[-beam_size:]
            arg0 = all_arg // beam_size  # previous id in batch
            arg1 = all_arg % beam_size
            prev_tok = tok_argsort[arg0, arg1]  # current word
            prev_state = outputs[2][arg0]
            score = tok_argsort_score[arg0, arg1]

            neos = neos[arg0] & (prev_tok != ID_EOS)

            ret_t = []
            for j in range(beam_size):
                ret_t.append(ret[arg0[j]] + [prev_tok[j]])

            ret = ret_t
        return ret[-1]

    def add_pad(self, data, fixlen):
        data = map(lambda x: x + [ID_PAD] * (fixlen - len(x)), data)
        data = list(data)
        return np.asarray(data)

    def get_batch(self, data, bucket_id):
        encoder_inputs, decoder_inputs = [], []
        encoder_len, decoder_len = [], []

        # Get a random batch of encoder and decoder inputs from data,
        # and add GO to decoder.
        for _ in range(self.batch_size):
            encoder_input, decoder_input = random.choice(data[bucket_id])

            encoder_inputs.append(encoder_input)
            encoder_len.append(len(encoder_input))

            decoder_inputs.append(decoder_input)
            decoder_len.append(len(decoder_input))

        batch_enc_len = max(encoder_len)
        batch_dec_len = max(decoder_len)

        encoder_inputs = self.add_pad(encoder_inputs, batch_enc_len)
        decoder_inputs = self.add_pad(decoder_inputs, batch_dec_len)
        encoder_len = np.asarray(encoder_len)
        # decoder_input has both <GO> and <EOS>
        # len(decoder_input)-1 is number of steps in the decoder.
        decoder_len = np.asarray(decoder_len) - 1

        return encoder_inputs, decoder_inputs, encoder_len, decoder_len
