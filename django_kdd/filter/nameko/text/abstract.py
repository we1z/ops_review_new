from calculate import text_classification_api
from tqdm import tqdm
from filter.nameko.db_connection import mongodb_connection,mongodb_connection_local
import html



# def get_text(html_text):
#     article = Article(url='', language='zh')
#     article.download(input_html=html_text)
#     article.parse()
#     text=article.text.split('\n')
#     # text.extend(article.images)
#     return text


def get_abstract(keyword,type):
    final=[]
    text_list=[html.unescape(i['text']).split('\n') for i in mongodb_connection('corpus')['classification'].find({'keyword':keyword,'type':type})]

    total=[]
    for i in text_list:
        total.extend(i)
    total=list(set(total))

    mission_list = [total[i:i + 100] for i in range(0, len(total), 100)]
    for i in tqdm(mission_list):
        classification_result=text_classification_api(i)[0]
        print(classification_result)
        for j in range(0, len(classification_result)):
            if classification_result[j] == 1.0:
                final.append(i[j])

    final=[i.strip() for i in final if i !='']

    zip=len(final)/len(total)
    print(zip)

    mongodb_connection('corpus')['summarization'].insert({'title':keyword,'type':type,'text':final,'zip':zip})

