# -*- coding:utf-8 -*-
'''
读取tensorflow模型，识别文本类型
'''
import os
import time
import numpy as np
import tensorflow as tf
from text import data_helpers
from gensim.models import Word2Vec
import sys
from config import *
# logger = data_helpers.logger_fn('tflog', 'test-{}.log'.format(time.asctime()))



tf.flags.DEFINE_string("checkpoint_dir", MODEL_DIR, "Checkpoint directory from training run")

# Model Hyperparameters
tf.flags.DEFINE_integer("pad_seq_len", 1500, "Recommand padding Sequence length of data (depends on the data)")
tf.flags.DEFINE_integer("embedding_dim", 200, "Dimensionality of character embedding (default: 128)")
tf.flags.DEFINE_integer("embedding_type", 1, "The embedding type (default: 1)")
tf.flags.DEFINE_string("filter_sizes", "3,4,5", "Comma-separated filter sizes (default: '3,4,5')")
tf.flags.DEFINE_integer("num_filters", 128, "Number of filters per filter size (default: 128)")
tf.flags.DEFINE_float("dropout_keep_prob", 0.5, "Dropout keep probability (default: 0.5)")
tf.flags.DEFINE_float("l2_reg_lambda", 0.0, "L2 regularization lambda (default: 0.0)")

# Misc Parameters
tf.flags.DEFINE_boolean("allow_soft_placement", True, "Allow device soft device placement")
tf.flags.DEFINE_boolean("log_device_placement", False, "Log placement of ops on devices")
tf.flags.DEFINE_boolean("gpu_options_allow_growth", True, "Allow gpu options growth")

FLAGS = tf.flags.FLAGS
FLAGS._parse_flags()
dilim = '-' * 100

# Load word2vec model file
assert os.path.isfile(word2vec_file)
model = Word2Vec.load(word2vec_file)

checkpoint_file = tf.train.latest_checkpoint(FLAGS.checkpoint_dir)
# logger.info(checkpoint_file)

text_graph = tf.Graph()

text_sess = tf.Session(graph=text_graph)
# with sess.as_default():
# Load the saved meta graph and restore variables
with text_graph.as_default():
    text_saver = tf.train.import_meta_graph("{}.meta".format(checkpoint_file))
    text_saver.restore(text_sess, checkpoint_file)


def text_classification_api(text):
    '''
    文字识别api
    :param text:文本内容，list
    :return: 文字类型，0为无关，1为市场相关，2为竞品相关
    '''

    test_data = data_helpers.load_single_data_list_without_label(text, model)

    x_test_front, y_test = data_helpers.pad_data(test_data, FLAGS.pad_seq_len)

    # Get the placeholders from the graph by name
    input_x_front = text_graph.get_operation_by_name("input_x_front").outputs[0]
    # input_x_behind = graph.get_operation_by_name("input_x_behind").outputs[0]

    # input_y = graph.get_operation_by_name("input_y").outputs[0]
    dropout_keep_prob = text_graph.get_operation_by_name("dropout_keep_prob").outputs[0]

    predictions = text_graph.get_operation_by_name("output/predictions").outputs[0]
    topKPreds = text_graph.get_operation_by_name("output/topKPreds").outputs[0]

    all_predictions = []
    all_topKPreds = []

    feed_dict = {
        input_x_front: x_test_front,
        dropout_keep_prob: 1.0
    }

    batch_predictions = text_sess.run(predictions, feed_dict)
    all_predictions = np.concatenate([all_predictions, batch_predictions])

    batch_topKPreds = text_sess.run(topKPreds, feed_dict)
    all_topKPreds = np.append(all_topKPreds, batch_topKPreds)

    return all_predictions, all_topKPreds


if __name__=="__main__":
    pass
