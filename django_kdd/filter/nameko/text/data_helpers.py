# -*- coding:utf-8 -*-

import jieba
from tflearn.data_utils import to_categorical, pad_sequences
import re
import Levenshtein
import os
import redis
from config import write_path


html_prefix = '''
<html>
<head>
    <meta charset="utf-8">
    <link rel="stylesheet" type="text/css" href="http://106.75.91.135:8080/static/css/style-af077fae60.min.css">
    <link rel="icon" href="http://106.75.91.135:8080/static/img/head.jpg" type="image/x-icon">
    <style type="text/css">
        iframe {
            width: -webkit-fill-available;
            height: 750px;
        }

        img {
            display: block;
        }
    </style>
</head>
'''


navbar='''
<div role="main" class="container">
    <nav>
        <a class="btn btn-lg btn-default" href="http://106.75.91.135:8080/filter/home">返回主页 &raquo;</a>
        <a class="btn btn-lg btn-default" onclick="window.location.href=window.location.href.replace('{}','{}')">{} &raquo;</a>
    </nav>
    <br>
    <h4>☃.完成度{}%</h4>
    <h4>☃.若未100%完成，刷新即可加载最新资讯</h4>
</div>
<body>
<main role="main" class="container">
'''

html_template = '''

<div>
<h1>{}</h1>
<div>{}</div>
<br>
<a class="btn btn-sm btn-default" href={}>原文链接</a>
</div>
<br>
'''

iframe_template = '''

<div>
<h1>{}</h1>
<iframe src={}></iframe>
<br>
<a class="btn btn-sm btn-default" href={}>原文链接</a>
</div>
<br>
'''

html_postfix = '''

    <nav>
        <a class="btn btn-lg btn-default" href="http://106.75.91.135:8080/filter/home">返回主页 &raquo;</a>
        <a class="btn btn-lg btn-default"
           onclick="window.location.href=window.location.href.replace('{}','{}')">{} &raquo;</a>
    </nav>
    <br>
    <h4>☃.完成度{}%</h4>
    <h4>☃.若未100%完成，刷新即可加载最新资讯</h4>
        <br>
        <br>
        <br>
    </main>
</body>
</html>
'''

pool = redis.ConnectionPool(host='localhost', port=6379, db=0)
redis_conn = redis.StrictRedis(connection_pool=pool)


def load_single_data_without_label(text, model):
    vocab = dict([(k, v.index) for (k, v) in model.wv.vocab.items()])

    def token_to_index(content):
        result = []
        for item in content.split(" "):
            id = vocab.get(item)
            if id is None:
                id = 0
                if not item:
                    id=3492

                # 对感兴趣的数字进行特殊处理
                elif re.match('^\d+(年|月|日)$', item):
                    id = 9143
                elif re.match('^\d+\.\d+$', item):
                    id = 39938
                elif re.match('^\d{1}$', item):
                    id = 888
                elif re.match('^\d{2}$', item):
                    id = 11376
                elif re.match('^\d{3}$', item):
                    id = 14637
                elif re.match('^\d{4}$', item):
                    id = 10537
                elif re.match('^\d+$', item):
                    id = 69384
                elif item == '%':
                    id = 28317
            result.append(id)
        return result

    front_content_indexlist = []

    content = ' ' + ' '.join(jieba.cut(text)) + ' '
    filtrate = re.compile(u'\s[^\u4E00-\u9FA50-9%.A-Za-z]+\s')

    content = filtrate.sub('  ', content)
    filtrate3 = re.compile(u'\s[^\u4E00-\u9FA50-9%.]+\s')
    content = filtrate3.sub('  ', content)
    content = content[1:-1]
    front_content_indexlist.append(token_to_index(content))

    class Data:
        def __init__(self):
            pass

        @property
        def number(self):
            return len(front_content_indexlist)

        @property
        def labels(self):
            return [0]

        @property
        def front_tokenindex(self):
            return front_content_indexlist

    return Data()


def pad_data(data, max_seq_len):
    """
    Padding each sentence of research data according to the max sentence length.
    Returns the padded data and data labels.
    :param data: The research data
    :param max_seq_len: The max sentence length of research data
    :returns: The padded data and data labels
    """
    data_front = pad_sequences(data.front_tokenindex, maxlen=max_seq_len, value=0.)
    # data_behind = pad_sequences(data.behind_tokenindex, maxlen=max_seq_len, value=0.)
    labels = to_categorical(data.labels, nb_classes=3)
    return data_front, labels


def load_single_data_list_without_label(text_list, word2vec_model):
    """
    Create the research data tokenindex based on the word2vec model file.
    Returns the class Data(includes the data tokenindex and data labels).
    :param input_file: The research data
    :param word2vec_model: The word2vec model file
    :return: The class Data(includes the data tokenindex and data labels)
    """
    vocab = dict([(k, v.index) for (k, v) in word2vec_model.wv.vocab.items()])

    def token_to_index(content):
        result = []
        for item in content.split(" "):
            id = vocab.get(item)
            if id is None:
                id = 0
                if not item:
                    id=3492

                # 对感兴趣的数字进行特殊处理
                elif re.match('^\d+(年|月|日)$', item):
                    id = 9143
                elif re.match('^\d+\.\d+$', item):
                    id = 39938
                elif re.match('^\d{1}$', item):
                    id = 888
                elif re.match('^\d{2}$', item):
                    id = 11376
                elif re.match('^\d{3}$', item):
                    id = 14637
                elif re.match('^\d{4}$', item):
                    id = 10537
                elif re.match('^\d+$', item):
                    id = 69384
                elif item == '%':
                    id = 28317
            result.append(id)
        return result

    front_content_indexlist = []
    behind_content_indexlist = []

    for content in text_list:
        content = ' ' + ' '.join(jieba.cut(content)) + ' '
        filtrate = re.compile(u'\s[^\u4E00-\u9FA50-9%.A-Za-z]+\s')

        content = filtrate.sub('  ', content)
        filtrate3 = re.compile(u'\s[^\u4E00-\u9FA50-9%.]+\s')
        content = filtrate3.sub('  ', content)
        content = content[1:-1]

        front_content_indexlist.append(token_to_index(content))

    class Data:
        def __init__(self):
            pass

        @property
        def number(self):
            return len(front_content_indexlist)

        @property
        def labels(self):
            return [0]

        @property
        def front_tokenindex(self):
            return front_content_indexlist

            # @property
            # def behind_tokenindex(self):
            #     return behind_content_indexlist

    return Data()


def is_similar_text(text_list, text):
    '''
    判断类型的新闻是否已经存在
    :param text_list:已经判断好的text列表
    :param text:需要判断的text
    :return:True or False
    '''
    if not text_list:
        return False
    for i in text_list:
        distance = Levenshtein.distance(i, text)
        if distance < max(len(text), len(i)) - min(len(text), len(i)) * 1.2 or distance < 0.4 * max(len(text), len(i)):
            return True
    return False


def write_html_file(content_list, type, keyword, keyword_state=0.0):
    '''
    写html文件
    :param content_list:写入html的内容
    :param type: type of doc
    :param keyword:关键词
    :param keyword_state:当前状态
    :return:
    '''
    html_content = ''

    for i in content_list:
        if i['type'] != 3.0:
            html_content += html_template.format(i['title'],i['text'],i['url'])
        else:
            html_content += iframe_template.format(i['title'],i['url'],i['url'])

    assert type in (1.0, 2.0)

    if type == 1.0:
        file_name = '/market-' + keyword + '.html'

        html_content = html_prefix+navbar.format('market','competitor','跳转到竞品分析',str(keyword_state*100)) + html_content + html_postfix.format('market','competitor','跳转到竞品分析',str(keyword_state*100))

        if os.path.exists(write_path + file_name):
            with open(write_path + file_name, mode='w') as f:
                f.write(html_content)
        else:
            redis_conn.srem('keyword', keyword)
            try:
                with open(write_path + file_name, mode='x') as f:
                    f.write(html_content)
            except FileExistsError:
                with open(write_path + file_name, mode='w') as f:
                    f.write(html_content)
    else:
        file_name = '/competitor-' + keyword + '.html'

        html_content = html_prefix+navbar.format('competitor','market','跳转到行研分析',str(keyword_state*100)) + html_content + html_postfix.format('competitor','market','跳转到行研分析',str(keyword_state*100))

        if os.path.exists(write_path + file_name):
            with open(write_path + file_name, mode='w', encoding='utf-8') as f:
                f.write(html_content)
        else:
            redis_conn.srem('keyword', keyword)
            try:
                with open(write_path + file_name, mode='x', encoding='utf-8') as f:
                    f.write(html_content)
            except FileExistsError:
                with open(write_path + file_name, mode='w', encoding='utf-8') as f:
                    f.write(html_content)
