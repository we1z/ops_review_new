from nameko.rpc import rpc

class GreetingService:
    name = "greeting_service"

    @rpc
    def hello(self, name):
        return "Hello, {}!".format(name)

from selenium.webdriver.common.by import By
from selenium import webdriver
import re

driver = webdriver.Chrome(r'C:\Program Files (x86)\Google\Chrome\Application\chromedriver.exe')
url = r'https://dumps.wikimedia.org/other/pagecounts-raw/'
driver.get(url)
content = driver.page_source
index_regex = re.compile(r'^\d{4}$')
res = index_regex.findall(content)
print(res)
link = driver.find_element(By.PARTIAL_LINK_TEXT, r'\d{4}')
print(link)
