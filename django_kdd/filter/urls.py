from django.conf.urls import url
from . import views
from django_kdd import settings
from django.views.static import serve



urlpatterns = [
    url(r'home', views.index),
    url(r'article_writing', views.article_writing),
    url(r'register', views.register),
    url(r'log_in', views.log_in),
    url(r'result', views.result),
    url(r'flowing_text', views.flowing_text),
    url(r'test', views.test),
    url(r'review_text', views.review_text),
    url(r'review_index', views.review_index),
    url(r'article_submit',views.article_submit,),
    url(r'^upload/$', views.upload_file, name='upload_file'),
    url(r'^upload/(?P<path>.*)$', serve, {'document_root': settings.MEDIA_ROOT}),
    url(r'^logout', views.logout),
    url(r'user_page', views.user_page),
    url(r'delete_review', views.delete_review),
    url(r'download_pdf', views.download_pdf),
]