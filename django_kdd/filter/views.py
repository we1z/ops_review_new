# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.shortcuts import render
from django.views.decorators.csrf import csrf_exempt
from django.http import HttpResponse
from filter.bridge.search import search_api
import json
import requests
from filter.nameko.db_connection import mongodb_connection
import time
from html import unescape
import os
from datetime import datetime
import hashlib
from django.http import HttpResponseRedirect, HttpResponse,FileResponse
import random
import pdfkit


def auth(request):
    """
    验证页面的cookies
    :param request:
    :return:
    """
    ticket = request.COOKIES.get('ticket')
    username = request.COOKIES.get('udfuenf')
    password = request.COOKIES.get('pdfuenf')
    if not ticket:
        return False
    elif not username:
        return False
    elif not password:
        return False
    if mongodb_connection('corpus')['users'].count({'ticket': ticket, 'username': username, 'password': password}) > 0:
        if time.time() > mongodb_connection('corpus')['users'].find_one(
                {'ticket': ticket, 'username': username, 'password': password})['cookies_expired']:
            return False
        else:
            return True
    else:
        return False


def index(request):
    """
    主页
    :param request:
    :return:
    """
    if auth(request):
        return render(request, 'home.html')
    else:
        return HttpResponseRedirect('log_in.html')


# @csrf_exempt
# def bar(request):
#     keyword = request.POST['keyword']
#     company = request.POST['company']
#     search_api(keyword, company)
#     return render(request, 'qiaodoumadai.html', {'Keyword':json.dumps(keyword)})


# @csrf_exempt
# def percent(request):
#     keyword = request.POST['keyword']
#     data = {'keyword': keyword}
#     r = requests.post('http://localhost:8006/nameko_percent', json=data)
#
#     num = r.json()
#     per = int(num['percent']*100)
#     res = {'percent': per, 'keyword': keyword}
#     res = json.dumps(res)
#     return HttpResponse(res)


@csrf_exempt
def result(request):
    """
    搜索关键字后的中转页面,包含行研分析,竞品分析和文摘生成
    :param request:
    :return:
    """
    if auth(request):
        keyword = request.POST['keyword']
        company = request.POST['company']
        data = {'company': company, 'keyword': keyword}
        requests.post('http://localhost:8006/nameko_dispatch_keyword', json=data)
        requests.post('http://localhost:8006/nameko_percent', json=data)
        return render(request, 'result.html', {'keyword': keyword})
    else:
        return HttpResponseRedirect('log_in.html')


@csrf_exempt
def test(request):
    """
    文摘页面
    :param request:
    :return:
    """
    if auth(request):
        keyword = request.GET['keyword']
        type = request.GET['type']
        is_regenerate = request.GET['is_regenerate']
        final_list = []
        requests.post('http://localhost:8006/dispatch_summary',
                      json={'keyword': keyword, 'type': type, 'is_regenerate': is_regenerate})
        if type == 'market':
            mongo_type = 1.0
            type_chinese = '行研'
        elif type == 'competitor':
            mongo_type = 2.0
            type_chinese = '竞品'
        else:
            mongo_type = None
        while mongodb_connection('corpus')['summarization'].count({'title': keyword, 'type': mongo_type}) == 0:
            time.sleep(3)
        l = [i for i in mongodb_connection('corpus')['summarization'].find({'title': keyword, 'type': mongo_type})][0]
        text_list = l['text']

        for i in text_list:
            final_list.append(i)

        return render(request, 'test.html',
                      {'blog_list': final_list, 'title': l['title'], 'type': str(type), 'type_chinese': type_chinese,
                       'keyword': keyword})
    else:
        return HttpResponseRedirect('log_in.html')


@csrf_exempt
def flowing_text(request):
    """
    动态版本的行研,竞品分析页面
    :param request:
    :return:
    """
    if auth(request):
        keyword = request.GET['keyword']
        type = request.GET['type']
        try:
            done_percent = mongodb_connection('corpus')['classification'].count(
                {'keyword': keyword, '$or': [{'state': 'done'}, {'state': 'fail'}]}) / mongodb_connection('corpus')[
                               'classification'].count({'keyword': keyword})
        except ZeroDivisionError:
            done_percent = 0
        per = int(done_percent * 100)
        if type == 'market':
            mongo_type = 1.0
            opposite = 'competitor'
            opposite_chinese = '竞品'
        elif type == 'competitor':
            mongo_type = 2.0
            opposite = 'market'
            opposite_chinese = '行研'
        text_list = [{'title': i['title'], 'text': unescape(i['article_html']), 'url': i['url']} for i in
                     mongodb_connection('corpus')['classification'].find({'keyword': keyword, 'type': mongo_type})]
        return render(request, 'flowing_text.html',
                      {'text_list': text_list, 'keyword': keyword, 'type': type, 'percent': per, 'opposite': opposite,
                       'opposite_chinese': opposite_chinese})
    else:
        return HttpResponseRedirect('log_in.html')


@csrf_exempt
def upload_file(request):
    # 拿到文件，保存在指定路径
    imgFile = request.FILES.get('upload')
    filename = str(float(time.time()))
    if os.path.exists('/data/upload/'):
        pass
    else:
        os.mkdir('/data/upload/')
    with open('/data/upload/{}'.format(filename), 'wb')as f:
        for chunk in imgFile.chunks():
            f.write(chunk)
    response = {
        "uploaded": 1,
        "fileName": filename,
        "url": 'http://106.75.91.135/upload/{}'.format(filename)
    }
    return HttpResponse(json.dumps(response))


@csrf_exempt
def article_writing(request):
    """
    在线写评审
    :param request:
    :return:
    """
    if auth(request):
        company = request.GET.get('company')
        content = mongodb_connection('corpus')['enterprise_review'].find_one({'company': company})
        return render(request, 'article_writing.html', content)
    else:
        return HttpResponseRedirect('log_in.html')


@csrf_exempt
def article_submit(request):
    """
    提交和预览在线评审页面
    :param request:
    :return:
    """
    if auth(request):
        text = {}
        text['title'] = request.POST['title']
        text['author'] = request.POST['author']
        text['company'] = request.POST['company']
        text['keyword'] = request.POST['keyword']
        text['company_financing_records'] = request.POST['company_financing_records']
        text['company_products'] = request.POST['company_products']
        text['company_problems'] = request.POST['company_problems']
        text['company_competitor'] = request.POST['company_competitor']
        text['company_market'] = request.POST['company_market']
        text['company_stock'] = request.POST['company_stock']
        text['company_team'] = request.POST['company_team']
        text['company_suggestions'] = request.POST['company_suggestions']
        text['company_intellectual_property'] = request.POST['company_intellectual_property']
        text['company_capital_change'] = request.POST['company_capital_change']
        text['company_stockholder_change'] = request.POST['company_stockholder_change']
        text['company_summary'] = request.POST['company_summary']
        text['company_latest_stock'] = request.POST['company_latest_stock']
        text['company_basic_info'] = request.POST['company_basic_info']
        text['company_news'] = request.POST['company_news']
        text['company_introduction'] = request.POST['company_introduction']
        text['upload_time'] = datetime.now().strftime('%Y-%m-%d')
        text['username'] = request.COOKIES.get('udfuenf')
        flag = False
        if request.POST['view_or_save'] == '1':
            text['index'] = True
            while flag == False:
                if mongodb_connection('corpus')['enterprise_review'].count(
                        {'company': text['company'], 'username': text['username']}) == 0:
                    try:
                        mongodb_connection('corpus')['enterprise_review'].insert(text)
                        mongodb_connection('corpus')['enterprise_review_backup'].insert(text)
                        flag = True
                    except:
                        pass
                else:
                    try:
                        mongodb_connection('corpus')['enterprise_review'].remove(
                            {'company': text['company'], 'username': text['username']})
                        mongodb_connection('corpus')['enterprise_review'].insert(text)
                        mongodb_connection('corpus')['enterprise_review_backup'].remove(
                            {'company': text['company'], 'username': text['username']})
                        mongodb_connection('corpus')['enterprise_review_backup'].insert(text)
                        flag = True
                    except:
                        pass
            return render(request, 'review_text.html', {'text': text})

        elif request.POST['view_or_save'] == '2':
            text['index'] = False
            while flag == False:
                if mongodb_connection('corpus')['enterprise_review'].count(
                        {'company': text['company'], 'username': text['username']}) == 0:
                    try:
                        mongodb_connection('corpus')['enterprise_review'].insert(text)
                        flag = True
                    except:
                        pass
                else:
                    try:
                        mongodb_connection('corpus')['enterprise_review'].remove(
                            {'company': text['company'], 'username': text['username']})
                        mongodb_connection('corpus')['enterprise_review'].insert(text)
                        flag = True
                    except:
                        pass
            return HttpResponseRedirect('user_page.html')
        else:
            text['index'] = False
            while flag == False:
                if mongodb_connection('corpus')['enterprise_review'].count(
                        {'company': text['company'], 'username': text['username']}) == 0:
                    try:
                        mongodb_connection('corpus')['enterprise_review'].insert(text)
                        flag = True
                    except:
                        pass
                else:
                    try:
                        mongodb_connection('corpus')['enterprise_review'].remove(
                            {'company': text['company'], 'username': text['username']})
                        mongodb_connection('corpus')['enterprise_review'].insert(text)
                        flag = True
                    except:
                        pass
            return render(request, 'review_text.html', {'text': text})
    else:
        return HttpResponseRedirect('log_in.html')


@csrf_exempt
def review_index(request):
    """
    企业初审目录
    :param request:
    :return:
    """
    if auth(request):
        text_list = [i for i in mongodb_connection('corpus')['enterprise_review'].find({'index': True})]
        return render(request, 'review_index.html', {'text_list': text_list})
    else:
        return HttpResponseRedirect('log_in.html')


@csrf_exempt
def review_text(request):
    """
    具体的企业初审页面
    :param request:
    :return:
    """
    if auth(request):
        company = request.GET['company']
        text = mongodb_connection('corpus')['enterprise_review'].find_one({'company': company})
        return render(request, 'review_text.html', {'text': text})
    else:
        return HttpResponseRedirect('log_in.html')


def md5(string):
    """
    md5加密
    :param string: 账号和密码
    :return: 加密后的账号和密码
    """
    hl = hashlib.md5()
    hl.update(string.encode(encoding='utf-8'))
    return hl.hexdigest()


def get_ticket():
    """
    生成cookies
    :return: 绑定到浏览器上的cookies
    """
    string = str(time.time() - random.randint(0, 10000))
    sha256 = hashlib.sha256()
    sha256.update(string.encode('utf-8'))
    res = sha256.hexdigest()
    return res


@csrf_exempt
def log_in(request):
    """

    :param request:
    :return:
    """
    if request.method == 'GET':
        return render(request, 'log_in.html', {'default': True})
    if request.method == 'POST':

        # 如果登录成功，绑定参数到cookie中，set_cookie
        username = request.POST.get('username')
        password = request.POST.get('password')
        # 查询用户是否在数据库中
        if mongodb_connection('corpus')['users'].count({'username': md5(username)}) > 0:
            if mongodb_connection('corpus')['users'].count({'username': md5(username), 'password': md5(password)}) > 0:
                ticket = get_ticket()
                # 绑定令牌到cookie里面
                response = HttpResponseRedirect('home.html')
                response.set_cookie('ticket', ticket, max_age=10000)
                response.set_cookie('pdfuenf', md5(password), max_age=10000)
                response.set_cookie('udfuenf', md5(username), max_age=10000)
                # 存在服务端
                mongodb_connection('corpus')['users'].update({'username': md5(username), 'password': md5(password)}, {
                    '$set': {'ticket': ticket, 'cookies_expired': time.time() + 3600 * 8}})
                return response
            else:
                return render(request, 'log_in.html', {'tip': '密码错误。'})
        else:
            return render(request, 'log_in.html', {'tip': '用户不存在。'})


@csrf_exempt
def register(request):
    """
    用户注册
    :param request:
    :return:
    """
    if request.method == 'GET':
        return render(request, 'register.html', {'default': True})
    if request.method == 'POST':
        username = request.POST.get('username')
        password = request.POST.get('password')
        r_password = request.POST.get('confirm_password')
        real_name = request.POST.get('real_name')
        username_encode = md5(username)
        info = check_register_form(username, password, r_password, username_encode, real_name)
        if info == True:
            password = md5(password)
            mongodb_connection('corpus')['users'].insert(
                {'username': username_encode, 'password': password, 'real_name': real_name,
                 'register_time': time.time()})
            return HttpResponseRedirect('log_in.html')
        else:
            return render(request, 'register.html', {'tip': info})


@csrf_exempt
def logout(request):
    """
    用户注销，删除cookies
    :param request:
    :return:
    """
    if request.method == 'GET':
        response = HttpResponseRedirect('log_in.html')
        response.delete_cookie('ticket')
        response.delete_cookie('pdfuenf')
        response.delete_cookie('udfuenf')

        return response


def check_register_form(ur, pw, r_pw, username_encode, realname):
    """

    :param ur: 用户名
    :param pw: 密码
    :param r_pw: 重复密码
    :param username_encode: md5加密后的用户名
    :param realname: 真实姓名
    :return: 填入django模板的内容
    """
    if len(ur) > 20:
        return ['danger', '账号过长。', 'info', '输入8到20位密码。', 'info', '重复确认密码。', 'info', '输入真实姓名。']
    elif len(ur) < 5:
        return ['danger', '账号太短。', 'info', '输入8到20位密码。', 'info', '重复确认密码。', 'info', '输入真实姓名。']
    elif mongodb_connection('corpus')['users'].count({'username': username_encode}) > 0:
        return ['danger', '账号已被注册。', 'info', '输入8到20位密码。', 'info', '重复确认密码。', 'info', '输入真实姓名。']
    for i in ur:
        if i not in 'qwertyuiopasdfghjklzxcvbnmQWERTYUIOPASDFGHJKLZXCVBNM1234567890_':
            return ['danger', '账号不能包含特殊字符。', 'info', '输入8到20位密码。', 'info', '重复确认密码。', 'info', '输入真实姓名。']
    if len(pw) > 20:
        return ['info', '输入5到20位账号，只能包含大小写字母，数字和下划线。', 'danger', '密码太长。', 'info', '重复确认密码。', 'info', '输入真实姓名。']
    elif len(pw) < 8:
        return ['info', '输入5到20位账号，只能包含大小写字母，数字和下划线。', 'danger', '密码太短。', 'info', '重复确认密码。', 'info', '输入真实姓名。']
    if pw != r_pw:
        return ['info', '输入5到20位账号，只能包含大小写字母，数字和下划线。', 'info', '输入8到20位密码。', 'danger', '密码不一致', 'info', '输入真实姓名。']
    if realname == '':
        return ['info', '输入5到20位账号，只能包含大小写字母，数字和下划线。', 'info', '输入8到20位密码。', 'info', '重复确认密码。', 'danger', '真实姓名不能为空。']
    for i in realname:
        if i in 'qwertyuiopasdfghjklzxcvbnmQWERTYUIOPASDFGHJKLZXCVBNM1234567890!@#$%^&*()_+=-[]{};:",./<>?':
            return ['info', '输入5到20位账号，只能包含大小写字母，数字和下划线。', 'info', '输入8到20位密码。', 'info', '重复确认密码。', 'danger',
                    '真实姓名只能是中文。']
    return True


@csrf_exempt
def user_page(request):
    """
    个人主页
    :param request:
    :return:
    """
    if auth(request):
        username = request.COOKIES.get('udfuenf')
        text_list = [i for i in mongodb_connection('corpus')['enterprise_review'].find({'username': username})]
        return render(request, 'user_page.html', {'text_list': text_list})
    else:
        return HttpResponseRedirect('log_in.html')


@csrf_exempt
def delete_review(request):
    """
    删除评审
    :param request:
    :return:
    """
    if auth(request):
        username = request.COOKIES.get('udfuenf')
        company = request.GET.get('company')
        mongodb_connection('corpus')['enterprise_review'].delete_one({'username': username, 'company': company})
        return HttpResponseRedirect('user_page.html')
    else:
        return HttpResponseRedirect('log_in.html')


@csrf_exempt
def download_pdf(request):
    if auth(request):
        css = '/home/yuanyuan/django_kdd/static/css/bootstrap.css'

        options = {
            'page-size': 'Letter',
            'margin-top': '0.75in',
            'margin-right': '0.75in',
            'margin-bottom': '0.75in',
            'margin-left': '0.75in',
            'encoding': "UTF-8",
        }

        cookies = {'udfuenf': request.COOKIES.get('udfuenf'),
                   'pdfuenf': request.COOKIES.get('pdfuenf'),
                   'ticket': request.COOKIES.get('ticket')
                   }

        url='http://106.75.91.135:8080/filter/review_text.html?company={}'.format(request.GET.get('company'))

        content=requests.get(url=url,cookies=cookies).text

        pdfkit.from_string(content, '/data/reviews/{}.pdf'.format(request.GET.get('company')), css=css, options=options)

        return 'http://106.75.91.135/reviews/{}.pdf'.format(request.GET.get('company'))
    else:
        return HttpResponseRedirect('log_in.html')

