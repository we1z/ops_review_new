import tensorflow as tf
learning_rate=1.0
size=1024
embsize=200
num_layers=1
data_dir="data"

# 用于存放训练好的模型的位置
train_dir="model1/"
tfboard="tfboard"
decode=True
geneos=True
max_gradient=1.0
batch_size=1
doc_vocab_size=32500
sum_vocab_size=30000
max_train=0
max_iter=1000000
steps_per_validation=1000
steps_per_checkpoint= 100
checkpoint="model/"


# We use a number of buckets for sampling
MARK_PAD = "<PAD>"
MARK_UNK1 = "<UNK1>"
MARK_UNK2 = "<UNK2>"
MARK_UNK3 = "<UNK3>"
MARK_UNK4 = "<UNK4>"
MARK_UNK5 = "<UNK5>"
MARK_UNK6 = "<UNK6>"
MARK_EOS = "<EOS>"
MARK_GO = "<GO>"
MARKS = [MARK_PAD, MARK_UNK1, MARK_UNK2, MARK_UNK3, MARK_UNK4, MARK_UNK5, MARK_UNK6, MARK_EOS, MARK_GO]
ID_PAD = 0
ID_UNK1 = 1
ID_UNK2 = 2
ID_UNK3 = 3
ID_UNK4 = 4
ID_UNK5 = 5
ID_UNK6 = 6
ID_EOS = 7
ID_GO = 8

lda_model_path='/home/j/文档/django_kdd/filter/nameko/summarization_text/lda_model/lda_model'
