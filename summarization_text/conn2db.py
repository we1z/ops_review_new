import pymongo
import time
from pymongo.errors import NetworkTimeout,ServerSelectionTimeoutError


# Mongodb配置
MONGO_SERVER = '106.75.91.135'
MONGO_PORT = 27017

# 数据库账户密码
CORPUS_DB_USER = 'UserCorpus'
CORPUS_DB_PW = '0onh2wsgzc'


def mongodb_connection(db):
    """
    获取数据库链接
    :param db: 数据库名称
    :return: 数据库链接
    """

    conn = pymongo.MongoClient(MONGO_SERVER, MONGO_PORT, connectTimeoutMS=5000, connect=False, maxPoolSize=3000,
                               wtimeoutMS=5000)
    try:
        conn.database.authenticate(CORPUS_DB_USER, CORPUS_DB_PW, db)
        corpus_db = conn[db]
        return corpus_db
    except NetworkTimeout or ServerSelectionTimeoutError:
        time.sleep(1)
        return mongodb_connection(db)

