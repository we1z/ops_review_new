from jieba import posseg as pseg
import re
import gensim
from multiprocessing import Pool
from gensim import corpora
from conn2db import mongodb_connection
from config import *


def printname(function):
    print('\033[1;32m {}\033[0m'.format(function.__name__))
    return function


def wash_word_posseg(doc_need_clean):
    """
    根据词性筛除部分词语
    :param doc_need_clean:待处理的语料
    :retrun str:处理完成的语料，便于下一过程的正则匹配，所以此处返回str形式的结果
    """
    result = []
    word_list = pseg.cut(doc_need_clean)
    regex = re.compile('(i|n|v|j)')
    for i in word_list:
        if regex.match(i.flag[0]):
            result.append(i.word)
    return ' '.join(result)


def wash_corpus(doc_need_clean):
    """
    过滤文本中的特殊字符
    ：param doc_need_clean:语料
    ：return:str:处理后的语料
    """
    regex = re.compile('[.,。，；‘’“”\n]')
    return regex.sub('', doc_need_clean)


def wash_single_character(doc_need_clean):
    """
    过滤分词后长度为一的词
    :param doc_need_clean:待处理的语料
    :return：list 处理后的语料
    """
    result = []
    for i in doc_need_clean:
        if len(i) != 1:
            result.append(i)
    return result


def get_corpus(no_and_num):
    """
    这是本模块的数据库数据入口
    获取训练集需要的文档材料，将文档分词存为list，每篇文档再存为一个list，最终返回代表文档集合的list
    因为是从数据库载入数据，为了实现每次能够录入不同的数据，设计了两个参数，每次从数据库中载入第no*num个之后的num个数据
    同时这样设计，可以更好地利用多进程，这一部分主要在训练模型部分使用，加速语料的载入过程，此事no最好设置为cpu的核数
    :param no:int 语料的批次，用于多进程，不可重复
    :param num:int 下载的语料数量
    :return list：文档集合
    """
    n = no_and_num[0]*no_and_num[1]
    corpus = mongodb_connection('corpus')
    data_set_from_mongo = corpus['classification'].find({'$or':[{'type': 1.0}, {'type': 2.0}]}).skip(n).limit(no_and_num[1])
    data_sets = []
    for data_set in data_set_from_mongo:
        doc_exam = data_set['text']
        #  词性
        doc_exam_index = wash_word_posseg(doc_exam)
        #  特殊字符
        doc_exam_before_split = wash_corpus(doc_exam_index)
        #  词长
        doc_exam_split = doc_exam_before_split.split()
        doc_exam_split = wash_single_character(doc_exam_split)
        data_sets.append(doc_exam_split)
    print(data_sets)
    return data_sets


def get_corpus_from_text_list(texts):
    """
    这是本模块从其他函数获取数据的数据入口，标准化后用于本模块其他函数
    这一部分可以优化，特殊字符匹配有些冗余，且涉及数据类型由list-str-list的转化，后续需改进
    :param texts: 文档列表list[str,]
    :return: 标准化后的语料输出
    """
    data_sets = []
    for data_set in texts:
        doc_exam = data_set
        #  词性
        doc_exam_index = wash_word_posseg(doc_exam)
        #  特殊字符
        doc_exam_before_split = wash_corpus(doc_exam_index)
        #  词长
        doc_exam_split = doc_exam_before_split.split()
        doc_exam_split = wash_single_character(doc_exam_split)
        data_sets.append(doc_exam_split)
    return data_sets


def get_doc_set(num_of_process,num):
    doc_set=[]
    pool=Pool(processes=num_of_process)
    for i in pool.map(get_corpus,[[i,num] for i in range(num_of_process)]):
        doc_set.append(i)
    return doc_set


def train_lda_model_api(num):
    """
    多进程训练lda模型,可以在这里设置模型的topic数量，还有数据
    :param num:
    :return:
    """
    num_of_process = 10
    doc_set = []
    doc_sets = []
    # for i in range(num_of_process):
    #     doc_set.append(p.apply_async(get_corpus, (i, num)))
    # p.close()
    # p.join()
    doc_set=get_doc_set(num_of_process,num)
    for item in doc_set:
        doc_sets.extend(item)
    dictionary = corpora.Dictionary(doc_sets)
    corpus_s = [dictionary.doc2bow(text) for text in doc_sets]
    lda_model = gensim.models.ldamodel.LdaModel(corpus_s,num_topics=10,id2word=dictionary,passes=500)
    file = lda_model_path
    lda_model.save(file)


def get_lda_model_api():
    """
    从文件中加载lda模型
    :return: lda模型
    """
    file = lda_model_path
    lda_model = gensim.models.LdaModel.load(file)
    return lda_model


def re_train_model_api(corpus, model):
    """
    使用新的语料来更新lda模型
    :param corpus: list[list[str,],]新语料
    :param model: 旧有lda模型
    :return: 新模型
    """
    dic = model.id2word
    gensim_corpus = [dic.doc2bow(text) for text in corpus]
    model.update(gensim_corpus)
    file = lda_model_path
    model.save(file)
    return model


def get_doc_topic_probability(doc, model):
    """
    获取单篇文档的主题分布
    :param doc:list[str,] 待判断主题分布的文章
    :param model: 模型
    :return: 主题分布概率
    """
    dic = model.id2word
    corpus = dic.doc2bow(doc)
    return model.get_document_topics(corpus)


def get_mult_doc_topic_probaility(doc_list):
    """
    获取一个文章集合中的每一篇文档的主题分布
    :param doc_lsit: 文档集
    :return: list[list[float,],]
    """
    doc_sets = get_corpus_from_text_list(doc_list)
    model = get_lda_model_api()
    result = []
    for doc in doc_sets:
        result.append(get_doc_topic_probability(doc, model))
    return result


if __name__ == '__main__':
    # 测试建立模型                             //测试通过
    train_lda_model_api(50)
    # 测试从文件中获取模型                     //测试通过
    # model = get_lda_model_api()
    # dic = model.id2word
    # print(dic)

    # 测试对模型进行更新,需要结合上面的测试，  //测试通过
    # data_test = get_corpus(1000, 2)F
    # re_train_model_api(data_test, model)

    # 测试通过lda模型获取一篇文章的主题分布    //测试通过
    # model = get_lda_model_api()
    # data_test = get_corpus([820,1])
    # print(get_doc_topic_probability(data_test[0],model))

    # 测试获取模型的主题数目                   //测试通过
    # model = get_lda_model_api()
    # print(model.get_topics())
    # print(len(model.get_topics()))

    # 测试获取每一个主题下的词                 //测试通过
    # model = get_lda_model_api()
    # print(model.print_topics(20,5))

    # 测试文档集的主题分布获取                 //测试通过
    # data_test = get_corpus([825, 5])
    # doc_test = []
    # for item in data_test:
    #     doc_tmp = ''.join(item)
    #     doc_test.append(doc_tmp)
    # res = get_mult_doc_topic_probaility(doc_test)
    # print(res)
    pass
