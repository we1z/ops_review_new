import logging
import os


def get_logger(name):
    """
    设置日志格式
    :param name: 日志名称
    :return:
    """
    program = os.path.basename(name)
    log = logging.getLogger(program)
    logging.basicConfig(format='%(asctime)s: %(levelname)s: %(message)s')
    logging.root.setLevel(level=logging.INFO)
    return log

def logger_fn(name, file, level=logging.INFO):
    """
    设置日志显示
    :param name:日志实例的名称
    :param file:文件日志
    :param level:指定日志的最低输出级别，默认为INFO
    :return:为logger添加的日志处理器
    """

    tf_logger = logging.getLogger(name)
    tf_logger.setLevel(level)
    fh = logging.FileHandler(file, mode='w', encoding='utf-8')
    formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s')
    fh.setFormatter(formatter)
    tf_logger.addHandler(fh)
    return tf_logger
