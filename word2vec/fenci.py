"""
对完成繁简体转换的文本进行分词，这里使用jieba 分词工具
"""
import jieba
output_f=open('wiki.zh.text.jian.fenci', 'a')
with open('wiki.zh.text.jian') as f:
    for i in f.readlines():
        output_f.write(" ".join(jieba.cut(i)).replace("  ", " ").replace("  ", " "))
        output_f.write('\n')

output_f.close()
