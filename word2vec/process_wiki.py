#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Author: Pan Yang (panyangnlp@gmail.com)
# Copyrigh 2017


from __future__ import print_function
from util.logger import get_logger
import six
import sys
from gensim.corpora import WikiCorpus


def get_wiki_word(file_in, file_out):
    """
     处理下载的维基语料，并将结果保存到指定位置
    :param file_in: 维基语料存放位置
    :param file_out: 处理结果存放位置
    :return:
    """
    space = " "
    i = 0
    output = open(file_out, 'w')
    wiki = WikiCorpus(file_in, lemmatize=False, dictionary={})
    for text in wiki.get_texts():
        if six.PY3:
            output.write(' '.join(text).decode('utf-8') + '\n')
        else:
            output.write(space.join(text) + "\n")
        i += 1
        if i % 10000 == 0:
            logger.info("Saved " + str(i) + " articles")
    output.close()
    logger.info("Finished Saved " + str(i) + " articles")


if __name__ == '__main__':
    logger = get_logger(sys.argv[0])
    logger.info("running %s" % ' '.join(sys.argv))

    # check and process input arguments
    if len(sys.argv) != 3:
        print("Using: python process_wiki.py enwiki.xxx.xml.bz2 wiki.en.text")
        sys.exit(1)

    inp, outp = sys.argv[1:3]
    get_wiki_word(inp, outp)
