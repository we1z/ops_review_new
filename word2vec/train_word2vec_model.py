# -*- coding: utf-8 -*-
# 使用处理好的语料训练词向量模型

from __future__ import print_function
from util.logger import get_logger
import sys
import multiprocessing
from gensim.models import Word2Vec
from gensim.models.word2vec import LineSentence


if __name__ == '__main__':
    logger = get_logger(sys.argv[0])
    logger.info("running %s" % ' '.join(sys.argv))

    # 检验输入格式是否正确
    if len(sys.argv) < 4:
        print("Useing: python train_word2vec_model.py input_text "
              "output_gensim_model output_word_vector")
        sys.exit(1)
    inp, outp1, outp2 = sys.argv[1:4]

    # 训练模型，并将模型保存到指定位置
    model = Word2Vec(LineSentence(inp), size=200, window=5, min_count=10, workers=multiprocessing.cpu_count())
    model.save(outp1)
    model.wv.save_word2vec_format(outp2, binary=False)
